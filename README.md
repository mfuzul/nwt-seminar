Croatia tickets
===============

This is a seminar in Advanced Web Technologies on the Faculty of Electrical Engineering, Mechanical Engineering and Naval Architecture, Split, Croatia.

The idea was to recreate a website called [Croatia Tickets](https://www.croatia-tickets.com) (last visited 2019-05-12) using modern technologies.

##There are three main parts of the project:
1. Front app 
    1. used to sell tickets for various types of events and visitable locations (e.g. theater, cinemas, museums, etc.)
1. Admin interface
    1. used to manage content for the **front app** through *CRUD operations* 
    1. displays various sales statistics and manages both admin and front app users
1. API 
    1. TODO

##Technologies:
1. Backend
    * Symfony 2.8.49
    * Various packages which are included in Symfony framework - e.g. Twig rendering engine on admin interface
1. Frontend 
    * React
    * Yarn package manager and various *JavaScript* libraries and plugins
    
##Installation steps

###Pre-installation

1. `git clone https://bitbucket.org/mfuzul/nwt-seminar.git`
1. `cd nwt-seminar/ && vagrant up`

#### Custom PuPHPet config (TODO)


### Installation
1. After Vagrant is finished, if **everything is ok**, use `vagrant ssh` to SSH into the machine
1. Once in the machine, run `cd /var/www/main/symfony` followed by `composer install` (just press `ENTER` when prompted to enter parameters)

###Post-installation

1. Download `parameters.yml` from [Google Drive](https://drive.google.com/open?id=1cooEXbICLwSfvKVQdgfK8PQ9mzAiO6Jl) (link last generated 2019-05-13)
1. Modify `hosts` file on your OS:
    * Unix based OS (Linux/MacOS):
        * `sudo nano /etc/hosts`

    * Windows OS:
        * Edit `Windows\System32\Drivers\etc\hosts` with **administrator** privileges

1. Copy the following contents into the hosts file:

```text
192.168.3.14 nwt.local www.nwt.local
192.168.3.14 admin.nwt.local www.admin.nwt.local
```

### Build assets
1. To build assets for **admin interface** run the following:
    1. `cd /var/www/main/symfony/src/AdminBundle/Resources/assets`
    1. `yarn encore production` or `yarn build`

### Known VM issues
