#!/usr/bin/env bash

ENVS=("dev" "test" "prod")
SYMFONY_PATH="/var/www/main/symfony/app/console"

in_array() {
    for e in $1; do
      if [[ "$e" == "$2" ]]; then
        return 1
      fi
    done

    return 0
}

clear_env() {
    if [[ -n $1 && 0 == "$(in_array $ENVS $1)" ]]; then
        echo "Unknown environment $1"
        exit
    fi

    php $SYMFONY_PATH c:c

    php $SYMFONY_PATH c:w

    php $SYMFONY_PATH c:c --no-debug

    php $SYMFONY_PATH c:w --no-debug
}

if [ $# -eq 0 ]; then
    clear_env
    exit
fi

while getopts "he:" OPTION
do
  echo $OPTION
  case $OPTION in
    h)
      echo "Command used to refresh Symfony cache from any place in the machine"
      echo "Options:"
      echo "-h --help - displays this message"
      echo "-e --env - used to clear/warmup single environment cache"
      echo "No arguments - clears/warmups cache for all environments"
      exit
      ;;
    e)
      clear_env $OPTARG
      exit
      ;;
  esac
done
