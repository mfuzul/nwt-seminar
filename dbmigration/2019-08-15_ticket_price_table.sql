/**
 * @author z3r0
 *
 * Add ticket price table.
 */

CREATE TABLE ticket_price
(
  `id`              INT(11) UNSIGNED PRIMARY KEY AUTO_INCREMENT,
  `content_id`      INT(11) UNSIGNED                NOT NULL,
  `ticket_group_id` INT(11) UNSIGNED                NOT NULL,
  `price`           DOUBLE PRECISION(4, 2) UNSIGNED NOT NULL,
  FOREIGN KEY fk_content (`content_id`) REFERENCES content (`id`),
  FOREIGN KEY fk_ticket_group (`ticket_group_id`) REFERENCES ticket_group (`id`)
);