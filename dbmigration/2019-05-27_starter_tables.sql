/**
 * @author mfuzul
 *
 * Startup tables.
 */

CREATE TABLE country
(
  `id`         INT(11) UNSIGNED PRIMARY KEY AUTO_INCREMENT,
  `name`       VARCHAR(100) NOT NULL,
  `short_code` VARCHAR(5)   NOT NULL,
  `ts_create`  DATETIME     DEFAULT NOW(),
  `ts_update`  DATETIME     DEFAULT NULL ON UPDATE NOW()
);

CREATE TABLE location
(
  `id`         INT(11) UNSIGNED PRIMARY KEY AUTO_INCREMENT,
  `country_id` INT(11) UNSIGNED NOT NULL,
  `name`       VARCHAR(100) NOT NULL,
  `latitude`   DOUBLE PRECISION(9, 7) NOT NULL,
  `longitude`  DOUBLE PRECISION(9, 7) NOT NULL,
  `ts_create`  DATETIME     DEFAULT NOW(),
  `ts_update`  DATETIME     DEFAULT NULL ON UPDATE NOW(),
  FOREIGN KEY fk_country(`country_id`) REFERENCES country(`id`)
);

CREATE TABLE content_type
(
  `id`        INT(11) UNSIGNED PRIMARY KEY AUTO_INCREMENT,
  `name`      VARCHAR(100) NOT NULL,
  `ts_create` DATETIME     DEFAULT NOW(),
  `ts_update` DATETIME     DEFAULT NULL ON UPDATE NOW()
);

CREATE TABLE content
(
  `id`              INT(11) UNSIGNED PRIMARY KEY AUTO_INCREMENT,
  `location_id`     INT(11) UNSIGNED NOT NULL,
  `content_type_id` INT(11) UNSIGNED NOT NULL,
  `name`            VARCHAR(255) NOT NULL,
  `address`         VARCHAR(255) NOT NULL,
  `latitude`        DOUBLE PRECISION(9, 7) NOT NULL,
  `longitude`       DOUBLE PRECISION(9, 7) NOT NULL,
  `description`     TEXT         NOT NULL,
  `ts_create`       DATETIME     DEFAULT NOW(),
  `ts_update`       DATETIME     DEFAULT NULL ON UPDATE NOW(),
  FOREIGN KEY fk_location(`location_id`) REFERENCES location(`id`),
  FOREIGN KEY fk_content_type(`content_type_id`) REFERENCES content_type(`id`)
);

CREATE TABLE content_image
(
  `id`         INT(11) UNSIGNED PRIMARY KEY AUTO_INCREMENT,
  `content_id` INT(11) UNSIGNED NOT NULL,
  `file_name`  VARCHAR(255) NOT NULL,
  `ts_create`  DATETIME DEFAULT NOW(),
  `ts_update`  DATETIME DEFAULT NULL ON UPDATE NOW(),
  FOREIGN KEY fk_content(`content_id`) REFERENCES content(`id`)
)

CREATE TABLE operational_interval
(
  `id`         INT(11) UNSIGNED PRIMARY KEY AUTO_INCREMENT,
  `content_id` INT(11) UNSIGNED NOT NULL,
  `from`       DATETIME NOT NULL,
  `to`         DATETIME NOT NULL,
  `ts_create`  DATETIME NOT NULL DEFAULT NOW(),
  `ts_update`  DATETIME DEFAULT NULL ON UPDATE NOW(),
  FOREIGN KEY fk_content(`content_id`) REFERENCES content(`id`)
);

CREATE TABLE working_hour
(
  `id`                      INT(11) UNSIGNED PRIMARY KEY AUTO_INCREMENT,
  `operational_interval_id` INT(11) UNSIGNED NOT NULL,
  `from`                    TIME     NOT NULL,
  `to`                      TIME     NOT NULL,
  `operational_monday`      TINYINT(1) UNSIGNED DEFAULT 0,
  `operational_tuesday`     TINYINT(1) UNSIGNED DEFAULT 0,
  `operational_wednesday`   TINYINT(1) UNSIGNED DEFAULT 0,
  `operational_thursday`    TINYINT(1) UNSIGNED DEFAULT 0,
  `operational_friday`      TINYINT(1) UNSIGNED DEFAULT 0,
  `operational_saturday`    TINYINT(1) UNSIGNED DEFAULT 0,
  `operational_sunday`      TINYINT(1) UNSIGNED DEFAULT 0,
  `ts_create`               DATETIME NOT NULL DEFAULT NOW(),
  `ts_update`               DATETIME DEFAULT NULL ON UPDATE NOW(),
  FOREIGN KEY fk_operational_interval(`operational_interval_id`) REFERENCES operational_interval(`id`)
);

CREATE TABLE ticket_group
(
  `id`        INT(11) UNSIGNED PRIMARY KEY AUTO_INCREMENT,
  `name`      VARCHAR(100) NOT NULL,
  `ts_create` DATETIME     DEFAULT NOW(),
  `ts_update` DATETIME     DEFAULT NULL ON UPDATE NOW()
);

CREATE TABLE content_ticket_group
(
  `content_id`      INT(11) UNSIGNED,
  `ticket_group_id` INT(11) UNSIGNED,
  `ts_create`       DATETIME DEFAULT NOW(),
  `ts_update`       DATETIME DEFAULT NULL ON UPDATE NOW(),
  PRIMARY KEY pk_content_ticket_group(`content_id`, `ticket_group_id`),
  FOREIGN KEY fk_content(`content_id`) REFERENCES content(`id`),
  FOREIGN KEY fk_ticket_group(`ticket_group_id`) REFERENCES ticket_group(`id`)
);

CREATE TABLE ticket
(
  `id`              INT(11) UNSIGNED PRIMARY KEY AUTO_INCREMENT,
  `content_id`      INT(11) UNSIGNED NOT NULL,
  `user_id`         INT(11) NOT NULL,
  `ticket_group_id` INT(11) UNSIGNED NOT NULL,
  `price`           DOUBLE PRECISION(10, 2) UNSIGNED NOT NULL,
  `ts_create`       DATETIME NOT NULL DEFAULT NOW(),
  `ts_paid`         DATETIME DEFAULT NULL,
  `ts_update`       DATETIME DEFAULT NULL ON UPDATE NOW(),
  FOREIGN KEY fk_content(`content_id`) REFERENCES content(`id`),
  FOREIGN KEY fk_user(`user_id`) REFERENCES fos_user(`id`),
  FOREIGN KEY fk_ticket_group(`ticket_group_id`) REFERENCES ticket_group(`id`)
);