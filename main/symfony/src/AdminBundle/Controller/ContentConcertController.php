<?php
/**
 * Created by PhpStorm.
 * User: z3r0
 * Date: 15.08.19.
 * Time: 17:29
 */

namespace AdminBundle\Controller;

use AdminBundle\Model\ContentFetchListRecordsQueryObject;
use CoreLogic\Entity\ContentConcert;
use CoreLogic\Entity\ContentType;

/**
 * Class ContentConcertController
 * @package AdminBundle\Controller
 */
class ContentConcertController extends AbstractContentController
{
    /** {@inheritdoc} */
    protected function entityClass(): string
    {
        return ContentConcert::class;
    }

    /** {@inheritdoc} */
    protected function prepareTableRecords(): array
    {
        $records = $this->getDoctrine()
            ->getRepository($this->entityClass())
            ->fetchListRecords(new ContentFetchListRecordsQueryObject([
                'contentTypeId' => ContentType::CONTENT_TYPE_MUSEUM,
            ]));

        return $records;
    }

}