<?php
/**
 * Created by PhpStorm.
 * User: z3r0
 * Date: 15.08.19.
 * Time: 15:39
 */

namespace AdminBundle\Controller;

use CoreLogic\Entity\TicketGroup;

/**
 * Class TicketGroupController
 * @package AdminBundle\Controller
 */
class TicketGroupController extends AbstractAdminCrudController
{
    /** {@inheritdoc} */
    protected function entityClass(): string
    {
        return TicketGroup::class;
    }

    /** {@inheritdoc} */
    protected function tableColumns(): array
    {
        return [
            'name' => true,
            'content_count' => true,
            'ts_create' => true,
            'ts_update' => true,
        ];
    }

}