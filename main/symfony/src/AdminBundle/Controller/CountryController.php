<?php
/**
 * Created by PhpStorm.
 * User: z3r0
 * Date: 05.08.19.
 * Time: 17:49
 */

namespace AdminBundle\Controller;


use CoreLogic\Entity\Country;

class CountryController extends AbstractAdminCrudController
{
    /** {@inheritdoc} */
    protected function entityClass(): string
    {
        return Country::class;
    }

    /** {@inheritdoc} */
    protected function tableColumns(): array
    {
        return [
            'id' => true,
            'name' => true,
            'shortCode' => true,
        ];
    }

}