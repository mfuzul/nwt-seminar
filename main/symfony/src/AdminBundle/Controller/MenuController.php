<?php
/**
 * Created by PhpStorm.
 * User: z3r0
 * Date: 05.08.19.
 * Time: 12:05
 */

namespace AdminBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class MenuController
 * @package AdminBundle\Controller
 */
class MenuController extends Controller
{
    /**
     * @return Response
     */
    public function sidebarAction(): Response
    {
        return $this->render('@Admin/Menu/sidebar_menu.html.twig', [
            'menu' => $this->get('menu.generator.admin_sidebar')->generate(),
        ]);
    }

}