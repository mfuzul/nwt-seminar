<?php
/**
 * Created by PhpStorm.
 * User: z3r0
 * Date: 31.03.19.
 * Time: 13:05
 */

namespace AdminBundle\Controller;


use AdminBundle\Form\FormType\LoginFormType;
use FOS\UserBundle\Controller\SecurityController as FosUserSecurityController;

/**
 * Class SecurityController
 * @package AdminBundle\Controller
 */
class SecurityController extends FosUserSecurityController
{
    /** {@inheritdoc} */
    protected function renderLogin(array $data)
    {
        $form = $this->container->get('form.factory')
            ->create(LoginFormType::class);

        $data['form'] = $form->createView();

        return $this->container->get('templating')
            ->renderResponse('AdminBundle:Security:login.html.twig', $data);
    }
}