<?php
/**
 * Created by PhpStorm.
 * User: z3r0
 * Date: 24.03.19.
 * Time: 12:57
 */

namespace AdminBundle\Controller;


use AdminBundle\Configuration\CrudConfiguration;
use AdminBundle\Constants\Message;
use CoreLogic\Entity\User;
use Doctrine\DBAL\Exception\UniqueConstraintViolationException;
use Symfony\Component\Form\Form;

/**
 * Class UserController
 * @package AdminBundle\Controller
 */
class UserController extends AbstractAdminCrudController
{
    /** {@inheritdoc} */
    protected function entityClass(): string
    {
        return User::class;
    }

    /** {@inheritdoc} */
    protected function tableColumns(): array
    {
        return [
            'id' => true,
            'name' => true,
            'username' => true,
            'email'=> true,
            'roles' => true,
            'tags' => true,
            '' => true,
        ];
    }

    /** {@inheritdoc} */
    protected function prepareTableRecords(): array
    {
        $records = $this->getDoctrine()
            ->getRepository($this->entityClass())
            ->fetchListRecords();

        foreach ($records as &$record) {
            $record['roles'] = json_encode(unserialize($record['roles']));
        }

        return $records;
    }

}