<?php
/**
 * Created by PhpStorm.
 * User: z3r0
 * Date: 24.03.19.
 * Time: 21:01
 */

namespace AdminBundle\Controller;

use AdminBundle\Configuration\CrudConfiguration;
use AdminBundle\Constants\CrudAction;
use AdminBundle\Constants\Message;
use AdminBundle\Exceptions\NotCrudOperationException;
use CoreLogic\Entity\EntityInterface;
use DateTime;
use Doctrine\DBAL\Exception\UniqueConstraintViolationException;
use Symfony\Component\Form\Form;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class AbstractAdminCrudController
 * @package AdminBundle\Controller
 */
abstract class AbstractAdminCrudController extends AbstractAdminController
{
    /** @var CrudConfiguration */
    protected static $crudConfiguration;

    /** @return string */
    abstract protected function entityClass(): string;

    /** @return array */
    abstract protected function tableColumns(): array;

    /**
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function tableRecordsAction(Request $request): JsonResponse
    {
        $records = $this->prepareTableRecords();

        return new JsonResponse($records);
    }

    /**
     * @param Request $request
     *
     * @return Response
     */
    public function createAction(Request $request): Response
    {
        $crudConfiguration = $this->prepareCrudConfiguration();

        $form = $this->prepareFormForAction(CrudAction::CREATE, $crudConfiguration->entityClassName);

        $form->handleRequest($request);

        if (
            $form->isSubmitted() &&
            $form->isValid() &&
            $this->handleFormSubmitForAction(CrudAction::CREATE, $form, $crudConfiguration)
        ) {
            return $this->redirectToRoute('admin_list', [
                'entitySlug' => $crudConfiguration->entitySlug,
            ]);
        }

        $twigData = [
            'form'              => $form->createView(),
            'crudConfiguration' => $crudConfiguration,
        ];

        $this->appendTwigDataForAction(CrudAction::CREATE, $twigData);

        $twigTemplate = $this->get('templating')->exists("AdminBundle:Crud/{$crudConfiguration->entityClassName}:create.html.twig") ?
            "AdminBundle:Crud/{$crudConfiguration->entityClassName}:create.html.twig" :
            'AdminBundle:Crud:create.html.twig';

        return $this->render($twigTemplate, $twigData);
    }

    /**
     * @param Request $request
     *
     * @return Response
     */
    public function listAction(Request $request): Response
    {
        $crudConfiguration = $this->prepareCrudConfiguration();

        return $this->render('@Admin/Crud/list.html.twig', [
            'crudConfiguration'       => $crudConfiguration,
            'tableColumns'            => $this->tableColumns(),
            'customColumnsFileExists' => file_exists(__DIR__ . "/../Resources/assets/js/DataTable/custom-column/{$crudConfiguration->entitySlug}.js"),
        ]);
    }

    /**
     * @param Request $request
     * @param int     $id
     *
     * @return Response
     */
    public function editAction(Request $request, int $id): Response
    {
        $crudConfiguration = $this->prepareCrudConfiguration();

        $entity = $this->getDoctrine()->getManager()->find($crudConfiguration->entityClass, $id);
        $form = $this->prepareFormForAction(
            CrudAction::EDIT,
            $crudConfiguration->entityClassName,
            $entity
        );

        $form->handleRequest($request);

        if (
            $form->isSubmitted() &&
            $form->isValid() &&
            $this->handleFormSubmitForAction(CrudAction::EDIT, $form, $crudConfiguration)
        ) {
            return $this->redirectToRoute('admin_list', [
                'entitySlug' => $crudConfiguration->entitySlug,
            ]);
        }

        $twigData = [
            'form'              => $form->createView(),
            'crudConfiguration' => $crudConfiguration,
        ];

        $this->appendTwigDataForAction(CrudAction::EDIT, $twigData);

        $twigTemplate = $this->get('templating')->exists("AdminBundle:Crud/{$crudConfiguration->entityClassName}:edit.html.twig") ?
            "AdminBundle:Crud/{$crudConfiguration->entityClassName}:edit.html.twig" :
            'AdminBundle:Crud:edit.html.twig';

        return $this->render($twigTemplate, $twigData);
    }

    /**
     * @param Request $request
     * @param int     $id
     *
     * @return Response
     */
    public function deleteAction(Request $request, int $id): Response
    {
        $crudConfiguration = $this->prepareCrudConfiguration();

        $doctrineManager = $this->getDoctrine()->getManager();
        if ($entity = $doctrineManager->find($crudConfiguration->entityClass, $id)) {
            $doctrineManager->remove($entity);
            $doctrineManager->flush();

            $this->addFlash(
                Message::SUCCESS,
                $this->get('translator.default')
                    ->trans(
                        'message.success.entity_deleted',
                        [
                            '%entityName%' => $crudConfiguration->entityClassName,
                        ]
                    )
            );
        } else {
            $this->addFlash(
                Message::ERROR,
                $this->get('translator.default')
                    ->trans(
                        'message.success.entity_does_not_exist',
                        [
                            '%entityName%' => $crudConfiguration->entityClassName,
                            '%id%' => $id,
                        ]
                    )
            );
        }

        return $this->redirectToRoute('admin_list', [
            'entitySlug' => $crudConfiguration->entitySlug,
        ]);
    }

    /**
     * @return array
     */
    protected function tableButtons(): array
    {
        return [
            'edit' => true,
            'delete' => true,
        ];
    }

    /** @return array */
    protected function prepareTableRecords(): array
    {
        $records = $this->getDoctrine()
            ->getRepository($this->entityClass())
            ->fetchListRecords();

        return $records;
    }

    /**
     * @param int    $actionType - @see \AdminBundle\Constants\CrudAction
     * @param string $entityClassName
     *
     * @return Form
     *
     * @throws NotCrudOperationException
     */
    protected function prepareFormForAction(int $actionType, string $entityClassName = '', EntityInterface $entity = null): Form
    {
        if (!in_array($actionType, CrudAction::ACTIONS)) {
            throw new NotCrudOperationException();
        }

        $className = "AdminBundle\Form\FormType\\{$entityClassName}FormType";

        switch ($actionType) {
            case CrudAction::CREATE:
                return $this->createForm($className);
            case CrudAction::EDIT:
                return $this->createForm($className, $entity);
        }
    }

    /**
     * @param int $actionType - @see \AdminBundle\Constants\CrudAction
     *
     * @param array $twigData
     */
    protected function appendTwigDataForAction(int $actionType, array &$twigData)
    {
    }

    /**
     * @param int               $actionType - @see \AdminBundle\Constants\CrudAction
     * @param Form              $form
     * @param CrudConfiguration $crudConfiguration
     *
     * @return bool
     *
     * @throws NotCrudOperationException
     */
    protected function handleFormSubmitForAction(int $actionType, Form $form, CrudConfiguration $crudConfiguration): bool
    {
        if (!in_array($actionType, CrudAction::ACTIONS)) {
            throw new NotCrudOperationException();
        }

        $entity = $form->getData();
        $doctrine = $this->getDoctrine()->getManager();

        switch ($actionType) {
            case CrudAction::CREATE:
                try {
                    $this->prePersistAlterEntityForCreateAction($entity);

                    $doctrine->persist($entity);
                    $doctrine->flush();

                    $this->addFlash(
                        Message::SUCCESS,
                        $this->get('translator.default')
                            ->trans(
                                'message.success.entity_added',
                                [
                                    '%entityName%' => $crudConfiguration->entityClassName,
                                ]
                            )
                    );

                    return true;
                    // TODO: 2019-04-09 @mfuzul - log the exception?
                } catch (UniqueConstraintViolationException $e) {
                    $this->addFlash(
                        Message::ERROR,
                        $this->get('translator.default')
                            ->trans(
                                'message.error.entity_exists',
                                [
                                    '%entityName%' => $crudConfiguration->entityClassName,
                                ]
                            )
                    );

                    return false;
                }
            case CrudAction::EDIT:
                $doctrine->persist($entity);
                $doctrine->flush();

                $this->addFlash(
                    Message::SUCCESS,
                    $this->get('translator.default')
                        ->trans(
                            'message.success.entity_edited',
                            [
                                '%entityName%' => $crudConfiguration->entityClassName,
                            ]
                        )
                );

                return true;
        }
    }

    /**
     * @param EntityInterface $entity
     */
    protected function prePersistAlterEntityForCreateAction(EntityInterface $entity)
    {
        if (method_exists($entity, 'setTsCreate')) {
            $entity->setTsCreate(new DateTime());
        }
    }

    /**
     * NOTE(@mfuzul): do not call from constructor, Symfony Container not available
     *
     * @return CrudConfiguration
     */
    private function prepareCrudConfiguration(): CrudConfiguration
    {
        if (self::$crudConfiguration instanceOf CrudConfiguration) {
            return self::$crudConfiguration;
        }

        self::$crudConfiguration = new CrudConfiguration(
            $this->entityClass(),
            $this->tableColumns(),
            $this->tableButtons(),
            $this->get('transformer.string_case'),
            $this->get('router'),
            $this->get('translator.default')
        );

        return self::$crudConfiguration;
    }
}