<?php
/**
 * Created by PhpStorm.
 * User: z3r0
 * Date: 15.08.19.
 * Time: 16:34
 */

namespace AdminBundle\Controller;

/**
 * Class AbstractContentController
 * @package AdminBundle\Controller
 */
abstract class AbstractContentController extends AbstractAdminCrudController
{
    /** {@inheritdoc} */
    protected function tableColumns(): array
    {
        return [
            'id' => true,
            'name' => true,
            'content_type' => true,
            'location' => true,
            'address' => true,
            'coordinates' => true,
            'ts_create' => true,
            'ts_update' => true,
            '' => true,
        ];
    }

}