<?php
/**
 * Created by PhpStorm.
 * User: z3r0
 * Date: 15.08.19.
 * Time: 16:36
 */

namespace AdminBundle\Controller;

use CoreLogic\Entity\ContentType;

/**
 * Class ContentTypeController
 * @package AdminBundle\Controller
 */
final class ContentTypeController extends AbstractAdminCrudController
{
    /** {@inheritdoc} */
    protected function entityClass(): string
    {
        return ContentType::class;
    }

    /** {@inheritdoc} */
    protected function tableColumns(): array
    {
        return [
            'name' => true,
            'content_count' => true,
            'ts_create' => true,
            'ts_update' => true,
        ];
    }

}