<?php
/**
 * Created by PhpStorm.
 * User: z3r0
 * Date: 24.03.19.
 * Time: 12:55
 */

namespace AdminBundle\Controller;


use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class CrudRouteResolverController
 * @package AdminBundle\Controller
 */
final class CrudRouteResolverController extends Controller
{
    /**
     * @param Request $request
     * @param string $entitySlug
     *
     * @return Response
     */
    public function tableRecordsAction(Request $request, string $entitySlug): Response
    {
        $entityClassName = $this->get('transformer.string_case')->dashCaseToCamelCaseString($entitySlug);

        return $this->forward("AdminBundle:{$entityClassName}:tableRecords");
    }

    /**
     * @param Request $request
     * @param string  $entitySlug
     *
     * @return Response
     */
    public function createAction(Request $request, string $entitySlug): Response
    {
        $entityClassName = $this->get('transformer.string_case')->dashCaseToCamelCaseString($entitySlug);

        return $this->forward("AdminBundle:{$entityClassName}:create");
    }

    /**
     * @param Request $request
     * @param string  $entitySlug
     *
     * @return Response
     */
    public function listAction(Request $request, string $entitySlug): Response
    {
        $entityClassName = $this->get('transformer.string_case')->dashCaseToCamelCaseString($entitySlug);

        return $this->forward("AdminBundle:{$entityClassName}:list");
    }

    /**
     * @param Request $request
     * @param string  $entitySlug
     * @param int     $id
     *
     * @return Response
     */
    public function editAction(Request $request, string $entitySlug, int $id): Response
    {
        $entityClassName = $this->get('transformer.string_case')->dashCaseToCamelCaseString($entitySlug);

        return $this->forward("AdminBundle:{$entityClassName}:edit", [
            'id'              => $id,
        ]);
    }

    /**
     * @param Request $request
     * @param string  $entitySlug
     * @param int     $id
     *
     * @return Response
     */
    public function deleteAction(Request $request, string $entitySlug, int $id): Response
    {
        $entityClassName = $this->get('transformer.string_case')->dashCaseToCamelCaseString($entitySlug);

        return $this->forward("AdminBundle:{$entityClassName}:delete", [
            'id' => $id,
        ]);
    }
}