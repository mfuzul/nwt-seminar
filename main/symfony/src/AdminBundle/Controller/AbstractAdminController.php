<?php
/**
 * Created by PhpStorm.
 * User: z3r0
 * Date: 27.03.19.
 * Time: 09:34
 */

namespace AdminBundle\Controller;


use AdminBundle\Constants\RouteConstant;
use AdminBundle\Service\Breadcrumb\BreadcrumbCollection;
use AdminBundle\Service\Breadcrumb\Model\Breadcrumb;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class AbstractAdminController
 * @package AdminBundle\Controller
 */
class AbstractAdminController extends Controller
{
    /**
     * @param Request $request
     *
     * @return Response
     */
    public function breadcrumbAction(Request $request): Response
    {
        $breadcrumbCollection = BreadcrumbCollection::getInstance();

        // Initial breadcrumb
        $breadcrumbCollection->addBreadcrumb(new Breadcrumb(
            $this->generateUrl('admin_home'),
            $this->get('translator')->transChoice('admin.dashboard', 1)
        ));

        if (RouteConstant::HOME !== $this->get('request_stack')->getMasterRequest()->get('_route')) {
            $this->resolveBreadcrumbs($this->get('request_stack')->getMasterRequest());
        }

        return $this->render('@Admin/Component/breadcrumbs.html.twig', [
            'breadcrumbs' => $breadcrumbCollection->all(),
        ]);
    }

    /**
     * @param Request $masterRequest
     */
    private function resolveBreadcrumbs(Request $masterRequest)
    {
        $breadcrumbCollection = BreadcrumbCollection::getInstance();
        $route = $masterRequest->get('_route');

        $breadcrumbLabel = "admin.{$route}";
        if ($entitySlug = $masterRequest->get('entitySlug')) {
            $entityUnderScoreCase = $this->get('transformer.string_case')->dashCaseToUnderScoreCaseString($entitySlug);

            if (in_array($route, RouteConstant::CRUD) && RouteConstant::LIST !== $route) {
                $link = $this->generateUrl(RouteConstant::LIST, [
                    'entitySlug' => $entitySlug,
                ]);
            } else {
                $link = '';
            }

            $breadcrumbCollection->addBreadcrumb(
                new Breadcrumb(
                    $link,
                    $this->get('translator')->transChoice("admin.{$entityUnderScoreCase}", 1)
                )
            );

            $breadcrumbCollection->addBreadcrumb(
                new Breadcrumb(
                    '',
                    $this->get('translator')->trans($breadcrumbLabel)
                )
            );
        } else {
            $breadcrumbCollection->addBreadcrumb(
                new Breadcrumb(
                    $this->generateUrl($route),
                    $breadcrumbLabel
                )
            );
        }
    }

}