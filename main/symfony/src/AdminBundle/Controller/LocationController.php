<?php
/**
 * Created by PhpStorm.
 * User: z3r0
 * Date: 05.08.19.
 * Time: 18:53
 */

namespace AdminBundle\Controller;

use CoreLogic\Entity\Location;

/**
 * Class LocationController
 * @package AdminBundle\Controller
 */
class LocationController extends AbstractAdminCrudController
{
    /** {@inheritdoc} */
    protected function entityClass(): string
    {
        return Location::class;
    }

    /** {@inheritdoc} */
    protected function tableColumns(): array
    {
        return [
            'name' => true,
            'country' => true,
            'coordinates' => true,
            '' => true,
        ];
    }

    /** {@inheritdoc} */
    protected function tableButtons(): array
    {
        return [
            'edit' => true,
            'delete' => true,
        ];
    }

}