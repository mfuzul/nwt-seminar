<?php
/**
 * Created by PhpStorm.
 * User: z3r0
 * Date: 15.08.19.
 * Time: 17:06
 */

namespace AdminBundle\Model;

/**
 * Class AbstractFetchListRecordsQueryObject
 */
abstract class AbstractFetchListRecordsQueryObject
{
    /**
     * AbstractFetchListRecordsQueryObject constructor.
     *
     * @param $data
     */
    public function __construct(array $data)
    {
        $objectVars = array_keys(get_object_vars($this));

        foreach ($data as $key => $value) {
            if (in_array($key, $objectVars)) {
                $this->{$key} = $value;
            }
        }
    }

}