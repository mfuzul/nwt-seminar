<?php
/**
 * Created by PhpStorm.
 * User: z3r0
 * Date: 15.08.19.
 * Time: 17:14
 */

namespace AdminBundle\Model;

/**
 * Class ContentFetchListRecordsQueryObject
 * @package AdminBundle\Model
 */
class ContentFetchListRecordsQueryObject extends AbstractFetchListRecordsQueryObject
{
    /** @var int */
    protected $contentTypeId;

    /**
     * @return int
     */
    public function getContentTypeId()
    {
        return $this->contentTypeId;
    }

}