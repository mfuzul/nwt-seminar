<?php
/**
 * Created by PhpStorm.
 * User: z3r0
 * Date: 20.04.19.
 * Time: 20:56
 */

namespace AdminBundle\Exceptions;

use Exception;
use Throwable;

/**
 * Class NotCrudOperationException
 * @package AdminBundle\Exceptions
 */
class NotCrudOperationException extends Exception
{
    /**
     * NotCrudOperationException constructor.
     *
     * @param string         $message
     * @param int            $code
     * @param Throwable|null $previous
     */
    public function __construct(string $message = "", int $code = 0, Throwable $previous = null)
    {
        if ('' === $message) {
            $message = "Not a CRUD operation.";
        }

        parent::__construct($message, $code, $previous);
    }
}