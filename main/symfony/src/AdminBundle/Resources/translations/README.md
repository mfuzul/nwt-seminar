# messages.yml readme

## Concept of naming messages

### Naming messages for CRUD breadcrumbs

1. Any message which includes a CRUD operation should be named as following:
`admin_{action}_{entitySlug}`