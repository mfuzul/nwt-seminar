const path = require('path');

module.exports = [
    {
        path: path.resolve(__dirname, './DataTable'),
        recursive: true,
        publicPrefix: 'js/DataTable/',
    },
    {
        path: path.resolve(__dirname, './admin'),
        recursive: true,
        publicPrefix: 'js/admin/',
    },
];