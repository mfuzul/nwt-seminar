const htmlGenerator = require('../../utilities/html-generator');
const columns = require('../config/default-column');

columns.render = (data, type, row, key) => {
    switch (key) {
        case 'name':
            return prepareName(row['fullName']);
        case 'roles':
            return prepareRoles(row[key]);
        case 'tags':
            return prepareTags(row);
        default:
            return row[key];
    }
};

columns.generateOptionButtons = (row) => {
    let html = '';
    const tableButtons = Object.entries(crudConfiguration.tableButtons);

    for (const [key, value] of tableButtons) {
        if (true === value) {
            switch (key) {
                case 'edit':
                    html += htmlGenerator.generateHtmlElement(
                        'a',
                        {
                            'href': columns.generateEntityPathWithId(entityEditPath, row['id']),
                            'class': 'btn btn-primary',
                        },
                        translator.trans(key)
                    );
                    break;
                case 'delete':
                    if (-1 === row['roles'].indexOf('SUPER_ADMIN')) {
                        html += htmlGenerator.generateHtmlElement(
                            'a',
                            {
                                'href': columns.generateEntityPathWithId(entityDeletePath, row['id']),
                                'class': 'btn btn-danger',
                            },
                            translator.trans(key)
                        );
                    }
                    break;
                default:
                    html += htmlGenerator.generateHtmlElement(
                        'a',
                        {
                            'href': '#',
                            'class': 'btn btn-default',
                        },
                        translator.trans(key)
                    );
            }
        }
    }

    return html;
};

const prepareName = (name) => {
    return name ? name : translator.trans('not_available');
};

const prepareRoles = (roles) => {
    roles = JSON.parse(roles);

    if (0 === roles.length) {
        return htmlGenerator.generateHtmlElement(
            'span',
            {
                'class': 'badge badge-primary'
            },
            'user'
        );
    }

    let html = '';

    for (const role of roles) {
        html += htmlGenerator.generateHtmlElement(
            'span',
            {
                'class': 'badge badge-primary'
            },
            role.slice(role.indexOf('_'))
                .replace(/_/g, ' ')
                .toLowerCase()
        );
    }

    return html;
};

const prepareTags = (row) => {
    let html = '';

    if (parseInt(row['enabled'], 10)) {
        html += htmlGenerator.generateHtmlElement(
            'span',
            {
                'class': 'badge badge-success',
            },
            translator.trans('enabled')
        );
    }
    if (parseInt(row['expired'], 10)) {
        html += htmlGenerator.generateHtmlElement(
            'span',
            {
                'class': 'badge badge-warning',
            },
            translator.trans('expired')
        );
    }
    if (parseInt(row['locked'], 10)) {
        html += htmlGenerator.generateHtmlElement(
            'span',
            {
                'class': 'badge badge-danger',
            },
            translator.trans('locked')
        );
    }
    if (parseInt(row['credentials_expired'], 10)) {
        html += htmlGenerator.generateHtmlElement(
            'span',
            {
                'class': 'badge badge-warning',
            },
            translator.trans('credentials_expired')
        );
    }

    return html;
};

module.exports = columns;