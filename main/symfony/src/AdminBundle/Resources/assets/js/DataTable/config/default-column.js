const htmlGenerator = require('../../utilities/html-generator');

let render = (data, type, row, key) => {
    return row[key];
};

let generateOptionButtons = (row) => {
    let html = '';
    const tableButtons = Object.entries(crudConfiguration.tableButtons);

    for (const [key, value] of tableButtons) {
        if (true === value) {
            switch (key) {
                case 'edit':
                    html += htmlGenerator.generateHtmlElement(
                        'a',
                        {
                            'href': generateEntityPathWithId(entityEditPath, row['id']),
                            'class': 'btn btn-primary',
                        },
                        translator.trans(key)
                    );
                    break;
                case 'delete':
                    html += htmlGenerator.generateHtmlElement(
                        'a',
                        {
                            'href': generateEntityPathWithId(entityDeletePath, row['id']),
                            'class': 'btn btn-danger',
                        },
                        translator.trans(key)
                    );
                    break;
                default:
                    html += htmlGenerator.generateHtmlElement(
                        'a',
                        {
                            'href': '#',
                            'class': 'btn btn-default',
                        },
                        translator.trans(key)
                    );
            }
        }
    }

    return html;
};

const generateEntityPathWithId = (path, id) => {
    if (path.indexOf('0') !== -1) {
        path.replace('0', id);
    } else {
        path += `/${id}`;
    }

    return path;
};

module.exports = {
    render,
    generateOptionButtons,
    generateEntityPathWithId
};