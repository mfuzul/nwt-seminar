require('../../node_modules/datatables/media/js/jquery.dataTables.min');

window.translator = require('./../utilities/translator');
translator.translations = crudConfiguration.translations;

const columns = customColumnsExist ?
    require(`./custom-column/${crudConfiguration.entitySlug}.js`) :
    require(`./config/default-column`);

$(document).ready(function() {
    if (!customDom) {
        customDom = '<"dataTableTop"ilf>rt<"dataTableBottom"p>';
    }

    const dataTableOptions = {
        'ajax': {
            dataSrc: '',
            url: crudConfiguration.tableRecordsPath,
        },
        'columnDefs': (() => {
            const tableColumns = Object.entries(crudConfiguration.tableColumns);
            let columnDefinitions = [];
            let target = 0;

            for (const [key, value] of tableColumns) {
                if (true === value) {
                    if ('' !== key) {
                        columnDefinitions.push({
                            'render': (data, type, row) => {
                                return columns.render(data, type, row, key);
                            },
                            'targets': target,
                        });
                    } else {
                        // Options go here if there are any
                        columnDefinitions.push({
                            'render': (data, type, row) => {
                                return columns.generateOptionButtons(row);
                            },
                            'targets': target,
                        });
                    }

                    target++;
                }
            }

            return columnDefinitions;
        })(),
        'dom': customDom,
    };

    const $dataTable = $("#DataTable");
    global.nwtDataTable = $dataTable.DataTable(dataTableOptions);

    $dataTable.find('input[type="search"]')
        .addClass('form-control');
});