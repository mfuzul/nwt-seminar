require('select2/dist/js/select2.full.min');

$(document).ready(() => {
    $('select[id*="roles"]').select2({
        multiple: true,
    });
});