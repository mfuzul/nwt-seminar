/**
 * Generates HTML element with the forwarded attributes and properties.
 *
 * @param {string}  tag
 * @param {object}  attr
 * @param {string}  inner
 * @param {boolean} singleTag
 *
 * @return {string}
 */
const generateHtmlElement = (tag, attr, inner = '', singleTag = false) => {
    let html = `<${tag}`;

    for (const key in attr) {
        html += ` ${key}="${attr[key]}" `;
    }

    html += singleTag ? '/>' : `>${inner}</${tag}>`;

    return html;
};

module.exports = {
    generateHtmlElement
};