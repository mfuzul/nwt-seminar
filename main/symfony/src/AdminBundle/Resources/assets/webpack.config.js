var Encore = require('@symfony/webpack-encore');
var entryGenerator = require('./entry-generator/entry-generator');

const jsEntries = entryGenerator.generateEntries(require('./js/file-watcher'), 'js');
for (let key in jsEntries) {
    Encore.addEntry(key, jsEntries[key]);
}

const lessEntries = entryGenerator.generateEntries(require('./less/file-watcher'), 'less');
for (let key in lessEntries) {
    Encore.addStyleEntry(key, lessEntries[key]);
}

Encore
    // set output and public path
    .setOutputPath('../../../../../public_html/admin/build/')
    .setPublicPath('/build')
    // set main entry point for shared js files
    .addEntry('js/main', './js/main.js')
    .addEntry('js/DataTable/init', './js/DataTable/init.js')
    // add main entry point for main custom css
    .addStyleEntry('css/main', './less/main.less')
    // enable LESS loader to process .less files
    .enableLessLoader()
    // auto provide jQuery for legacy plugins
    .autoProvidejQuery()
    // clean output folder before every build
    .cleanupOutputBeforeBuild()
    // disable generating of runtime.js file
    .disableSingleRuntimeChunk()
    // enable build notifications to know if build passed as it should have
    .enableBuildNotifications()
;

module.exports = Encore.getWebpackConfig();