const path = require('path');

module.exports = [
    {
        path: path.resolve(__dirname, './DataTable'),
        recursive: true,
        buildPrefix: 'less/DataTable/',
        publicPrefix: 'css/DataTable/',
    },
    {
        path: path.resolve(__dirname, './admin'),
        recursive: true,
        buildPrefix: 'less/admin/',
        publicPrefix: 'css/admin/',
    },
];