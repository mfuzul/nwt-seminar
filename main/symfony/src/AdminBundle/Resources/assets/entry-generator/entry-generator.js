const glob = require('glob');
const path = require('path');

let generator = {};

function stripExtensionFromFileName(fileName, extension) {
    return fileName.replace(`.${extension}`, '');
}

generator.generateEntries = function(entries, extension) {
    let generatedEntries = {};

    for (entry of entries) {
        const rootDir = entry.path;
        const recursivePath = entry.recursive ? '**/' : '';

        const publicPrefix = `./${entry.publicPrefix}`;
        let buildPrefix = publicPrefix;

        if (entry.hasOwnProperty('buildPrefix')) {
            buildPrefix = `./${entry.buildPrefix}`;
        }

        const matchedFiles = glob.sync(`${entry.path}/${recursivePath}*.${extension}`)
            .map(function(match) {
                return path.relative(rootDir, match);
            });

        for (matched of matchedFiles) {
            const extensionStrippedMatch = publicPrefix + stripExtensionFromFileName(matched, extension);

            generatedEntries[extensionStrippedMatch] = buildPrefix + matched;
        }
    }

    return generatedEntries;
};

module.exports = generator;