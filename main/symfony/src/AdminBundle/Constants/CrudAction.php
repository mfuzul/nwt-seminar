<?php
/**
 * Created by PhpStorm.
 * User: z3r0
 * Date: 20.04.19.
 * Time: 20:43
 */

namespace AdminBundle\Constants;

/**
 * Class CrudAction
 * @package AdminBundle\Constants
 */
class CrudAction
{
    /** @var int */
    const CREATE = 1;

    /** @var int */
    const LIST = 2;

    /** @var int */
    const EDIT = 3;

    /** @var int */
    const DELETE = 4;

    /** @var array */
    const ACTIONS = [
        self::CREATE,
        self::LIST,
        self::EDIT,
        self::DELETE,
    ];
}