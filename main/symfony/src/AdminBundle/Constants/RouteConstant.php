<?php
/**
 * Created by PhpStorm.
 * User: z3r0
 * Date: 28.03.19.
 * Time: 22:11
 */

namespace AdminBundle\Constants;


/**
 * Used as a holder for constants holding route names in admin system.
 *
 * Class RouteConstant
 * @package AdminBundle\Constants
 */
class RouteConstant
{
    /** @var string */
    const HOME = 'admin_home';

    /** @var string */
    const TABLE_RECORDS = 'admin_table_records';

    /** @var string */
    const CREATE = 'admin_create';

    /** @var string */
    const LIST = 'admin_list';

    /** @var string */
    const EDIT = 'admin_edit';

    /** @var string */
    const DELETE = 'admin_delete';

    /** @var array */
    const CRUD = [
        self::CREATE,
        self::LIST,
        self::EDIT,
        self::DELETE
    ];

}