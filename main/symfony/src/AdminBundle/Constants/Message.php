<?php
/**
 * Created by PhpStorm.
 * User: z3r0
 * Date: 09.04.19.
 * Time: 09:00
 */

namespace AdminBundle\Constants;


/**
 * Message constants for showing alerts below breadcrumbs.
 * Constants match CSS class of alert.
 *
 * Class Message
 * @package AdminBundle\Constants
 */
class Message
{
    /** @var string */
    const SUCCESS = 'success';

    /** @var string */
    const INFO = 'info';

    /** @var string */
    const WARNING = 'warning';

    /** @var string */
    const ERROR = 'danger';

}