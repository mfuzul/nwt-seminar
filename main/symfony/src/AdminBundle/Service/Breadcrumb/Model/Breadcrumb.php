<?php
/**
 * Created by PhpStorm.
 * User: z3r0
 * Date: 27.03.19.
 * Time: 09:31
 */

namespace AdminBundle\Service\Breadcrumb\Model;

/**
 * Class Breadcrumb
 * @package AdminBundle\Service\Breadcrumb\Model
 */
class Breadcrumb
{
    /** @var string */
    private $link;

    /** @var string */
    private $label;

    /**
     * Breadcrumb constructor.
     *
     * @param string $link
     * @param string $label
     */
    public function __construct(
        string $link,
        string $label
    ) {
        $this->link = $link;
        $this->label = $label;
    }

    /** @return string */
    public function getLink(): string
    {
        return $this->link;
    }

    /**
     * @param string $link
     *
     * @return Breadcrumb
     */
    public function setLink(string $link): self
    {
        $this->link = $link;

        return $this;
    }

    /** @return string */
    public function getLabel(): string
    {
        return $this->label;
    }

    /**
     * @param string $label
     *
     * @return Breadcrumb
     */
    public function setLabel(string $label): self
    {
        $this->label = $label;

        return $this;
    }
}