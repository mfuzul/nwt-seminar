<?php
/**
 * Created by PhpStorm.
 * User: z3r0
 * Date: 27.03.19.
 * Time: 09:38
 */

namespace AdminBundle\Service\Breadcrumb;


use AdminBundle\Service\Breadcrumb\Model\Breadcrumb;
use Iterator;

/**
 * Class BreadcrumbCollection
 * @package AdminBundle\Service\Breadcrumb
 */
class BreadcrumbCollection implements Iterator
{
    /**
     * @var BreadcrumbCollection
     */
    private static $instance;

    /** @var int */
    private $position;

    /** @var Breadcrumb[] */
    private $breadcrumbs;

    /** @return self */
    public static function getInstance(): self
    {
        if (null === self::$instance) {
            self::$instance = new BreadcrumbCollection();
        }

        return self::$instance;
    }

    /** BreadcrumbCollection constructor. */
    private function __construct()
    {
        $this->position = 0;
        $this->breadcrumbs = [];
    }

    /** @return Breadcrumb */
    public function current(): Breadcrumb
    {
        return $this->breadcrumbs[$this->position];
    }

    /** Increments position. */
    public function next()
    {
        ++$this->position;
    }

    /** @return int */
    public function key(): int
    {
        return $this->position;
    }

    /** @return bool */
    public function valid(): bool
    {
        return isset($this->breadcrumbs[$this->position]);
    }

    /** Reset position to 0. */
    public function rewind()
    {
        $this->position = 0;
    }

    /** @return Breadcrumb[] */
    public function all(): array
    {
        return $this->breadcrumbs;
    }

    /**
     * @param Breadcrumb $breadcrumb
     *
     * @return self
     */
    public function addBreadcrumb(Breadcrumb $breadcrumb): self
    {
        ++$this->position;

        $this->breadcrumbs[$this->position] = $breadcrumb;

        return $this;
    }

}