<?php
/**
 * Created by PhpStorm.
 * User: z3r0
 * Date: 15.08.19.
 * Time: 16:53
 */

namespace AdminBundle\Form\FormType;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class AbstractEntityType
 * @package AdminBundle\Form\FormType
 */
abstract class AbstractEntityType extends AbstractType
{
    abstract protected function entityClass(): string;

    /** {@inheritdoc} */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => $this->entityClass(),
        ]);
    }
}