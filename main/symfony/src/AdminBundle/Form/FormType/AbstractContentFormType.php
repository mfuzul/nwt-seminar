<?php
/**
 * Created by PhpStorm.
 * User: z3r0
 * Date: 15.08.19.
 * Time: 17:42
 */

namespace AdminBundle\Form\FormType;

use CoreLogic\Entity\Content;
use CoreLogic\Entity\ContentType;
use CoreLogic\Entity\Location;
use CoreLogic\Entity\TicketGroup;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;

/**
 * Class AbstractContentFormType
 * @package AdminBundle\Form\FormType
 */
abstract class AbstractContentFormType extends AbstractEntityType
{
    /** {@inheritdoc} */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        /** @var Content $content */
        $content = $builder->getData();
        $location = $content ? $content->getLocation() : null;

        $builder
            ->add('name', TextType::class, [
                'label' => 'form.name',
            ])
            ->add('location', EntityType::class, [
                'choice_label' => function (Location $location = null) {
                    return null !== $location ? $location->getName() : '';
                },
                'choice_value' => function (Location $location = null) {
                    return null !== $location ? $location->getId() : 0;
                },
                'class' => Location::class,
                'label' => 'form.location',
            ])
            ->add('contentType', EntityType::class, [
                'choice_label' => function (ContentType $contentType = null) {
                    return null !== $contentType ? $contentType->getName() : '';
                },
                'choice_value' => function (ContentType $contentType = null) {
                    return null !== $contentType ? $contentType->getId() : 0;
                },
                'class' => ContentType::class,
                'label' => 'form.content_type',
            ])
            ->add('address', TextType::class, [
                'label' => 'form.address',
            ])
            ->add('coordinates', TextType::class, [
                'data' => $location ? $location->getCoordinates() : '',
                'label' => 'form.coordinates',
                'mapped' => false,
            ])
            ->add('description', TextareaType::class, [
                'label' => 'form.description',
            ])
            ->add('ticketGroup', EntityType::class, [
                'choice_label' => function (TicketGroup $ticketGroup = null) {
                    return null !== $ticketGroup ? $ticketGroup->getName() : '';
                },
                'choice_value' => function (TicketGroup $ticketGroup = null) {
                    return null !== $ticketGroup ? $ticketGroup->getId() : 0;
                },
                'class' => TicketGroup::class,
                'label' => 'form.ticket_group',
                'multiple' => true,
            ])
        ;
    }

}