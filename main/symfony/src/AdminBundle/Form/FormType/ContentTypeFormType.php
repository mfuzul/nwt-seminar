<?php
/**
 * Created by PhpStorm.
 * User: z3r0
 * Date: 15.08.19.
 * Time: 16:52
 */

namespace AdminBundle\Form\FormType;

use CoreLogic\Entity\ContentType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;

/**
 * Class ContentTypeFormType
 * @package AdminBundle\Form\FormType
 */
class ContentTypeFormType extends AbstractEntityType
{
    /** {@inheritdoc} */
    protected function entityClass(): string
    {
        return ContentType::class;
    }

    /** {@inheritdoc} */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', TextType::class, [
                'label' => 'form.name',
            ])
        ;
    }

}