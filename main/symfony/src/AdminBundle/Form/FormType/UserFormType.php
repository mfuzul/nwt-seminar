<?php
/**
 * Created by PhpStorm.
 * User: z3r0
 * Date: 31.03.19.
 * Time: 13:02
 */

namespace AdminBundle\Form\FormType;

use CoreLogic\Constants\UserRole;
use CoreLogic\Entity\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class UserFormType
 * @package AdminBundle\Form\FormType
 */
class UserFormType extends AbstractType
{
    /** {@inheritdoc} */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('username', TextType::class, [
                'label' => 'form.username',
            ])
            ->add('email', EmailType::class, [
                'label' => 'form.email',
            ])
            ->add('firstName', TextType::class, [
                'label' => 'form.first_name',
                'required' => false,
            ])
            ->add('lastName', TextType::class, [
                'label' => 'form.last_name',
                'required' => false,
            ])
            ->add('plainPassword', RepeatedType::class, [
                'first_options' => [
                    'label' => 'form.password',
                ],
                'second_options' => [
                    'label' => 'form.repeat_password'
                ],
                'required' => false,
                'type' => PasswordType::class,
            ])
            ->add('enabled', CheckboxType::class, [
                'label' => 'form.enabled',
                'required' => false,
            ])
            ->add('locked', CheckboxType::class, [
                'label' => 'form.locked',
                'required' => false,
            ])
            ->add('roles', ChoiceType::class, [
                'choices' => array_combine(UserRole::ROLES, UserRole::ROLES),
                'label' => 'form.roles',
                'multiple' => true,
            ])
        ;
    }

    /** {@inheritdoc} */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => User::class,
        ]);
    }
}