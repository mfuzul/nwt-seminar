<?php
/**
 * Created by PhpStorm.
 * User: z3r0
 * Date: 05.08.19.
 * Time: 18:51
 */

namespace AdminBundle\Form\FormType;

use CoreLogic\Entity\Country;
use CoreLogic\Entity\Location;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class LocationFormType
 * @package AdminBundle\Form\FormType
 */
class LocationFormType extends AbstractType
{
    /** {@inheritdoc} */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        /** @var Location $location */
        $location = $builder->getData();

        $builder
            ->add('name', TextType::class, [
                'label' => 'form.name',
            ])
            ->add('country', EntityType::class, [
                'choice_label' => function(Country $country = null) {
                    return $country ? $country->getName() : '';
                },
                'choice_value' => function(Country $country = null) {
                    return $country ? $country->getId() : 0;
                },
                'class' => Country::class,
                'label' => 'form.country',
            ])
            ->add('coordinates', TextType::class, [
                'data' => $location ? $location->getCoordinates() : '',
                'label' => 'form.coordinates',
                'mapped' => false,
            ])
        ;

        $builder->addEventListener(FormEvents::POST_SUBMIT, function(FormEvent $event) {
            list($latitude, $longitude) = explode(',', $event->getForm()->get('coordinates')->getData());

            /** @var Location $entity */
            $entity = $event->getData();

            $entity->setLatitude(round((float) trim($latitude), 7));
            $entity->setLongitude(round((float) trim($longitude), 7));

            $event->setData($entity);
        });
    }

    /** {@inheritdoc} */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Location::class,
        ]);
    }

}