<?php
/**
 * Created by PhpStorm.
 * User: z3r0
 * Date: 15.08.19.
 * Time: 17:43
 */

namespace AdminBundle\Form\FormType;

use CoreLogic\Entity\ContentMuseum;

/**
 * Class ContentMuseumFormType
 * @package AdminBundle\Form\FormType
 */
class ContentMuseumFormType extends AbstractContentFormType
{
    /** {@inheritdoc} */
    protected function entityClass(): string
    {
        return ContentMuseum::class;
    }

}