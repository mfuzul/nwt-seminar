<?php
/**
 * Created by PhpStorm.
 * User: z3r0
 * Date: 09.04.19.
 * Time: 10:15
 */

namespace AdminBundle\Form\FormType;


use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;

/**
 * Class LoginFormType
 * @package AdminBundle\Form\FormType
 */
class LoginFormType extends AbstractType
{
    /** {@inheritdoc} */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('_username', TextType::class, [
                'label' => 'form.username',
            ])
            ->add('_password', PasswordType::class, [
                'label' => 'form.password',
            ])
            ->add('_remember_me', CheckboxType::class, [
                'label' => 'form.remember_me',
                'label_attr' => [
                    'class' => 'text-capitalize',
                ],
                'required' => false,
            ])
            ->add('_submit', SubmitType::class, [
                'attr' => [
                    'class' => 'btn btn-primary btn-block text-uppercase',
                ],
                'label' => 'form.login',
            ])
        ;
    }

    /**
     * NOTICE(2019-04-09, @mfuzul): override so fields have names given in builder
     *  and not the ones provided from parent @see AbstractType
     *
     * {@inheritdoc}
     */
    public function getName()
    {
        return null;
    }
}