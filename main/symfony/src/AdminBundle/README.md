# Admin bundle documentation

## 1. CRUD route resolver controller

`Controller\CrudRouteResolverController.php` is used as a route resolver with for basic CRUD operations on entities.
It holds `create`, `read (list)`, `update (edit)` and `delete` actions for resolving actions. The `Entity` which
is being operated on is resolved from an entity slug which is embedded in the URL.