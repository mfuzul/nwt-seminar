<?php
/**
 * Created by PhpStorm.
 * User: z3r0
 * Date: 27.03.19.
 * Time: 08:41
 */

namespace AdminBundle\Configuration;


use AdminBundle\Constants\RouteConstant;
use CoreLogic\Service\Transformer\StringCaseTransformer;
use CoreLogic\Utility\Traits\ArrayHelper;
use Symfony\Bundle\FrameworkBundle\Routing\Router;
use Symfony\Bundle\FrameworkBundle\Translation\Translator;

/**
 * Class CrudConfiguration
 * @package AdminBundle\Configuration
 */
class CrudConfiguration
{
    use ArrayHelper;

    /** @var StringCaseTransformer */
    private $slugTransformer;

    /** @var Router */
    private $router;

    /** @var Translator */
    private $translator;

    /**
     * Represents full entity class name (with namespace).
     *
     * @var string
     */
    public $entityClass;

    /**
     * Represents entity class name.
     *
     * @var string
     */
    public $entityClassName;

    /** @var string */
    public $entitySlug;

    /** @var string */
    public $entityUnderScoreCase;

    /** @var array */
    public $tableColumns;

    /** @var array */
    public $tableButtons;

    /** @var string */
    public $tableRecordsPath;

    /** @var string */
    public $createPath;

    /** @var string */
    public $listPath;

    /** @var string */
    public $editPath;

    /** @var string */
    public $deletePath;

    /** @var array */
    public $translations;

    /**
     * CrudConfiguration constructor.
     *
     * @param string          $entityClass
     * @param array           $tableColumns
     * @param array           $tableButtons
     * @param StringCaseTransformer $slugTransformer
     * @param Router          $router
     * @param Translator      $translator
     */
    public function __construct(
        string $entityClass,
        array $tableColumns,
        array $tableButtons,
        StringCaseTransformer $slugTransformer,
        Router $router,
        Translator $translator
    ) {
        $this->entityClass = $entityClass;
        $entityClassExploded = explode('\\', $entityClass);
        $this->entityClassName = array_pop($entityClassExploded);
        unset($entityClassExploded);

        $this->tableColumns = $tableColumns;
        $this->tableButtons = $tableButtons;

        $this->slugTransformer = $slugTransformer;
        $this->router = $router;
        $this->translator = $translator;

        $this->setPaths($entityClass);

        $this->entityUnderScoreCase = $this->slugTransformer->dashCaseToUnderScoreCaseString($this->entitySlug);

        $this->translations = $this->prepareTranslations();

        $this->unsetServices();
    }

    /**
     * Function used to set CRUD paths.
     *
     * @var string $entityClass
     */
    private function setPaths(string $entityClass)
    {
        $entityClassArray = explode('\\', $entityClass);
        $this->entitySlug = $this->slugTransformer->camelCaseToDashCaseString(
            array_pop($entityClassArray)
        );

        $this->tableRecordsPath = $this->router->generate(
            RouteConstant::TABLE_RECORDS,
            [
                'entitySlug' => $this->entitySlug,
            ]
        );

        $this->createPath = $this->router->generate(
            RouteConstant::CREATE,
            [
                'entitySlug' => $this->entitySlug,
            ]
        );

        $this->listPath = $this->router->generate(
            RouteConstant::LIST,
            [
                'entitySlug' => $this->entitySlug,
            ]
        );

        $this->editPath = $this->router->generate(
            RouteConstant::EDIT,
            [
                'entitySlug' => $this->entitySlug,
            ]
        );

        $this->deletePath = $this->router->generate(
            RouteConstant::DELETE,
            [
                'entitySlug' => $this->entitySlug,
            ]
        );
    }

    /** @return array */
    private function prepareTranslations(): array
    {
        $translations = [];

        foreach ($this->translator->getCatalogue()->all('messages') as $key => $value) {
            $key = explode('.', $key);

            if ('admin' !== $key[0]) {
                continue;
            }

            $translations[$this->last($key)] = $value;
        }

        return $translations;
    }

    /**
     * Unset all services used only in constructor.
     */
    private function unsetServices()
    {
        unset($this->slugTransformer);
        unset($this->router);
        unset($this->translator);
    }
}