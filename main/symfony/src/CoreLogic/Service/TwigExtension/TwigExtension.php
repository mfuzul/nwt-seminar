<?php
/**
 * Created by PhpStorm.
 * User: z3r0
 * Date: 31.03.19.
 * Time: 12:35
 */

namespace CoreLogic\Service\TwigExtension;


use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;

/**
 * Class TwigExtension
 * @package CoreLogic\Service\TwigExtension
 */
class TwigExtension extends AbstractExtension
{
    public function getFunctions()
    {
        return [
            new TwigFunction('ucfirst', [$this, 'upperCaseFirstLetter']),
            new TwigFunction('uniqid', [$this, 'uniqueId']),
        ];
    }

    /**
     * @param string $data
     *
     * @return string
     */
    public function upperCaseFirstLetter(string $data): string {
        return ucfirst($data);
    }

    /**
     * @param string $prefix
     * @param bool   $moreEntropy
     *
     * @return string
     */
    public function uniqueId(string $prefix = '', bool $moreEntropy = false): string
    {
        return uniqid($prefix, $moreEntropy);
    }

}