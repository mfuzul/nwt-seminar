<?php
/**
 * Created by PhpStorm.
 * User: z3r0
 * Date: 24.03.19.
 * Time: 20:38
 */

namespace CoreLogic\Service\Transformer;


/**
 * Class used for transforming slugs into wanted forms.
 *
 * Class StringCaseTransformer
 * @package CoreLogic\Service\Transformer
 */
class StringCaseTransformer
{
    /**
     * @param string $dashCaseString
     *
     * @return string
     */
    public function dashCaseToCamelCaseString(string $dashCaseString): string
    {
        if ('' === $dashCaseString) {
            return '';
        }

        $entityClassName = '';

        foreach (explode('-', $dashCaseString) as $dashCaseStringPart) {
            $entityClassName .= ucfirst($dashCaseStringPart);
        }

        return $entityClassName;
    }

    /**
     * @param string $camelCaseString
     *
     * @return string
     */
    public function camelCaseToDashCaseString(string $camelCaseString): string
    {
        preg_match_all('/[A-Z][a-z]+/', $camelCaseString, $matches);

        return strtolower(implode('-', array_shift($matches)));
    }

    /**
     * @param string $dashCaseString
     *
     * @return string
     */
    public function dashCaseToUnderScoreCaseString(string $dashCaseString): string
    {
        return preg_replace('/\-/', '_', $dashCaseString);
    }

    /**
     * @param string $underScoreCaseString
     *
     * @return string
     */
    public function underScoreCaseToCamelCaseString(string $underScoreCaseString): string
    {
        $camelCaseString = '';

        $underScoreCaseString = strtolower($underScoreCaseString);
        foreach (explode('_', $underScoreCaseString) as $i => $underScoreCaseStringPart) {
            $camelCaseString .= 0 === $i ? $underScoreCaseStringPart : ucfirst($underScoreCaseStringPart);
        }

        return $camelCaseString;
    }

}