<?php
/**
 * Created by PhpStorm.
 * User: z3r0
 * Date: 05.08.19.
 * Time: 12:52
 */

namespace CoreLogic\Service\Menu;

use CoreLogic\Constants\UserRole;

/**
 * Class AdminSidebarMenuGenerator
 * @package CoreLogic\Service\Menu
 */
class AdminSidebarMenuGenerator extends AbstractMenuGenerator
{
    /** {@inheritdoc} */
    protected function generateRoleUser(): array
    {
        $role = UserRole::ROLE_USER;

        return [
            new MenuItem(
                $this->translator->transChoice('admin.dashboard', 1),
                $this->router->generate('admin_home'),
                $role,
                'chart-bar'
            ),
        ];
    }

    /** {@inheritdoc} */
    protected function generateRoleSuperAdmin(): array
    {
        $role = UserRole::ROLE_SUPER_ADMIN;

        return [
            new MenuItem(
                $this->translator->transChoice('admin.user', 0),
                $this->router->generate('admin_list', ['entitySlug' => 'user']),
                $role,
                'user'
            ),
            new MenuItem(
                $this->translator->transChoice('admin.content', 1),
                '',
                $role,
                'table',
                new MenuItemCollection([
                    new MenuItem(
                        $this->translator->transChoice('admin.museum', 0),
                        $this->router->generate('admin_list', ['entitySlug' => 'content-museum']),
                        $role
                    ),
                    new MenuItem(
                        $this->translator->transChoice('admin.concert', 0),
                        $this->router->generate('admin_list', ['entitySlug' => 'content-concert']),
                        $role
                    ),
                ])
            ),
            new MenuItem(
                $this->translator->transChoice('admin.content_type', 0),
                $this->router->generate('admin_list', ['entitySlug' => 'content-type']),
                $role,
                'cog'
            ),
            new MenuItem(
                $this->translator->transChoice('admin.ticket', 0),
                '',
                $role,
                'ticket-alt',
                new MenuItemCollection([
                    new MenuItem(
                        $this->translator->transChoice('admin.find_ticket', 0),
                        '',
                        $role
                    ),
                    new MenuItem(
                        $this->translator->transChoice('admin.ticket_group', 0),
                        $this->router->generate('admin_list', ['entitySlug' => 'ticket-group']),
                        $role
                    ),
                ])
            ),
            new MenuItem(
                $this->translator->transChoice('admin.country', 0),
                $this->router->generate('admin_list', ['entitySlug' => 'country']),
                $role,
                'flag'
            ),
            new MenuItem(
                $this->translator->transChoice('admin.location', 0),
                $this->router->generate('admin_list', ['entitySlug' => 'location']),
                $role,
                'location-arrow'
            ),
        ];
    }

}