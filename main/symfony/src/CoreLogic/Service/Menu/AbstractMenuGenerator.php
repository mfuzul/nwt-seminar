<?php
/**
 * Created by PhpStorm.
 * User: z3r0
 * Date: 05.08.19.
 * Time: 12:52
 */

namespace CoreLogic\Service\Menu;

use CoreLogic\Constants\UserRole;
use CoreLogic\Service\Transformer\StringCaseTransformer;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\Translation\TranslatorInterface;

/**
 * Class AbstractMenuGenerator
 * @package CoreLogic\Service\Menu
 */
abstract class AbstractMenuGenerator
{
    /** @var TranslatorInterface */
    protected $translator;

    /** @var RouterInterface */
    protected $router;

    /** @var StringCaseTransformer */
    protected $stringCaseTransformer;

    /**
     * AbstractMenuGenerator constructor.
     *
     * @param TranslatorInterface   $translator
     * @param RouterInterface       $router
     * @param StringCaseTransformer $stringCaseTransformer
     */
    public function __construct(
        TranslatorInterface $translator,
        RouterInterface $router,
        StringCaseTransformer $stringCaseTransformer
    ) {
        $this->translator = $translator;
        $this->router = $router;
        $this->stringCaseTransformer = $stringCaseTransformer;
    }

    /**
     * @return MenuItemCollection
     */
    public function generate(): MenuItemCollection
    {
        $menuCollectionItems = [];

        /** @var string $role */
        foreach (UserRole::ROLES as $role) {
            $generateMethodName = "generate{$this->stringCaseTransformer->underScoreCaseToCamelCaseString($role)}";

            if (method_exists($this, $generateMethodName)) {
                $menuCollectionItems = array_merge($menuCollectionItems, $this->{$generateMethodName}());
            }
        }

        return new MenuItemCollection($menuCollectionItems);
    }

    /**
     * @return array
     */
    protected function generateRoleUser(): array
    {
        return [];
    }

    /**
     * @return array
     */
    protected function generateRoleOperatorMuseum(): array
    {
        return [];
    }

    /**
     * @return array
     */
    protected function generateRoleOperatorConcert(): array
    {
        return [];
    }

    /**
     * @return array
     */
    protected function generateRoleAdmin(): array
    {
        return [];
    }

    /**
     * @return array
     */
    protected function generateRoleSuperAdmin(): array
    {
        return [];
    }

}