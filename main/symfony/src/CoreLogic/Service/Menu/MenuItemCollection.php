<?php
/**
 * Created by PhpStorm.
 * User: z3r0
 * Date: 05.08.19.
 * Time: 12:40
 */

namespace CoreLogic\Service\Menu;

use Iterator;

/**
 * Class MenuItemCollection
 * @package CoreLogic\Service\Menu
 */
class MenuItemCollection implements Iterator
{
    /** @var int */
    private $position = 0;

    /** @var array */
    private $items;

    /**
     * MenuItemCollection constructor.
     *
     * @param array $items
     */
    public function __construct(array $items)
    {
        $this->position = 0;
        $this->items = $items;
    }

    /**
     * @return MenuItem
     */
    public function current(): MenuItem
    {
        return $this->items[$this->position];
    }

    /** {@inheritdoc} */
    public function next()
    {
        ++$this->position;
    }

    /** {@inheritdoc} */
    public function key(): int
    {
        return $this->position;
    }

    /** {@inheritdoc} */
    public function valid(): bool
    {
        return array_key_exists($this->position, $this->items) &&
            ($this->items[$this->position] instanceof MenuItem);
    }

    /** {@inheritdoc} */
    public function rewind()
    {
        $this->position = 0;
    }

}