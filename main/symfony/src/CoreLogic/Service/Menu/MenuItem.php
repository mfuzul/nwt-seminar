<?php
/**
 * Created by PhpStorm.
 * User: z3r0
 * Date: 05.08.19.
 * Time: 12:26
 */

namespace CoreLogic\Service\Menu;

use CoreLogic\Constants\UserRole;

/**
 * Class MenuItem
 * @package CoreLogic\Service\Menu
 */
class MenuItem
{
    /** @var string */
    private $text;

    /** @var string */
    private $path;

    /** @var string */
    private $role;

    /** @var string */
    private $icon;

    /** @var MenuItemCollection */
    private $subMenu;

    /**
     * MenuItem constructor.
     *
     * @param string             $text
     * @param string             $path
     * @param string             $role
     * @param string             $icon
     * @param MenuItemCollection $subMenu
     *
     * @throws \Exception
     */
    public function __construct(
        string $text,
        string $path,
        string $role,
        string $icon = '',
        MenuItemCollection $subMenu = null
    ) {
        $this->text = ucfirst(strtolower($text));
        $this->path = $path;
        if (in_array($role, UserRole::ROLES)) {
            $this->role = $role;
        } else {
            throw new \Exception("Unsupported role: {$role}");
        }
        $this->icon = $icon;
        $this->subMenu = $subMenu;
    }

    /**
     * @return string
     */
    public function getText(): string
    {
        return $this->text;
    }

    /**
     * @return string
     */
    public function getPath(): string
    {
        return $this->path;
    }

    /**
     * @return string
     */
    public function getRole(): string
    {
        return $this->role;
    }

    /**
     * @return string
     */
    public function getIcon(): string
    {
        return $this->icon;
    }

    /**
     * @return MenuItemCollection|null
     */
    public function getSubMenu()
    {
        return $this->subMenu;
    }

}