<?php
/**
 * Created by PhpStorm.
 * User: z3r0
 * Date: 15.08.19.
 * Time: 17:25
 */

namespace CoreLogic\Model;

/**
 * Class PdoBindValue
 * @package CoreLogic\Model
 */
final class PdoBindValue
{
    /** @var string */
    private $name;

    /** @var mixed */
    private $value;

    /** @var int */
    private $pdoParamType;

    /**
     * PdoBindValue constructor.
     *
     * @param string $name
     * @param        $value
     * @param int    $pdoParamType
     */
    public function __construct(
        string $name,
        $value,
        int $pdoParamType
    ) {
        $this->name = $name;
        $this->value = $value;
        $this->pdoParamType = $pdoParamType;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return mixed
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * @return int
     */
    public function getPdoParamType(): int
    {
        return $this->pdoParamType;
    }

}