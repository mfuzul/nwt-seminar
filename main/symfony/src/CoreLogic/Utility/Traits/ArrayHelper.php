<?php
/**
 * Created by PhpStorm.
 * User: z3r0
 * Date: 16.04.19.
 * Time: 08:31
 */

namespace CoreLogic\Utility\Traits;


/**
 * Trait ArrayHelper
 * @package CoreLogic\Utility\Traits
 */
trait ArrayHelper
{
    /**
     * @param array $array
     *
     * @return mixed
     */
    public function last(array $array)
    {
        return $array[count($array)-1];
    }
}