<?php
/**
 * Created by PhpStorm.
 * User: z3r0
 * Date: 05.08.19.
 * Time: 12:23
 */

namespace CoreLogic\Constants;

/**
 * Class UserRole
 * @package CoreLogic\Constants
 */
class UserRole
{
    /** @var string */
    const ROLE_USER = 'ROLE_USER';

    /** @var string */
    const ROLE_OPERATOR_MUSEUM = 'ROLE_OPERATOR_MUSEUM';

    /** @var string */
    const ROLE_OPERATOR_CONCERT = 'ROLE_OPERATOR_CONCERT';

    /** @var string */
    const ROLE_ADMIN = 'ROLE_ADMIN';

    /** @var string */
    const ROLE_SUPER_ADMIN = 'ROLE_SUPER_ADMIN';

    /** @var array */
    const ROLES = [
        self::ROLE_USER,
        self::ROLE_OPERATOR_MUSEUM,
        self::ROLE_OPERATOR_CONCERT,
        self::ROLE_ADMIN,
        self::ROLE_SUPER_ADMIN,
    ];

}