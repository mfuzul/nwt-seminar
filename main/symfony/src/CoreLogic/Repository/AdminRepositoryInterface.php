<?php
/**
 * Created by PhpStorm.
 * User: z3r0
 * Date: 05.08.19.
 * Time: 17:51
 */

namespace CoreLogic\Repository;

use AdminBundle\Model\AbstractFetchListRecordsQueryObject;

/**
 * Interface AdminRepositoryInterface
 * @package CoreLogic\Repository
 */
interface AdminRepositoryInterface
{
    /**
     * @param AbstractFetchListRecordsQueryObject|null $queryObject
     *
     * @return array
     */
    function fetchListRecords(AbstractFetchListRecordsQueryObject $queryObject = null): array;

}