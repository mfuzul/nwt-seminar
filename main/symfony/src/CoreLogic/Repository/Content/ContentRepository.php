<?php
/**
 * Created by PhpStorm.
 * User: z3r0
 * Date: 15.08.19.
 * Time: 17:09
 */

namespace CoreLogic\Repository\Content;

use AdminBundle\Model\AbstractFetchListRecordsQueryObject;
use CoreLogic\Entity\Content;
use CoreLogic\Model\PdoBindValue;
use CoreLogic\Repository\AdminRepositoryInterface;
use CoreLogic\Repository\NwtRepositoryExtension;
use PDO;

/**
 * Class ContentRepository
 * @package CoreLogic\Repository\Content
 */
final class ContentRepository extends NwtRepositoryExtension implements AdminRepositoryInterface
{
    /** {@inheritdoc} */
    protected function entityClass(): string
    {
        return Content::class;
    }

    /** {@inheritdoc} */
    public function fetchListRecords(AbstractFetchListRecordsQueryObject $queryObject = null): array
    {
        $whereQueryPart = [];
        $bindValues = [];
        if (null !== $queryObject) {
            $whereQueryPart[] = 'content.content_type_id = :contentTypeId';
            $bindValues[] = new PdoBindValue(':contentTypeId', $queryObject->getContentTypeId(), PDO::PARAM_INT);
        }

        if (!empty($whereQueryPart)) {
            $whereQueryPart = 'WHERE ' . implode(' AND ', $whereQueryPart);
        }

        $pdoPrep = $this->conn->prepare("
            SELECT
              content.id,
              content.name,
              content_type.name AS `content_type`,
              location.name AS `location`,
              content.address,
              CONCAT(
                content.latitude,
                ', ',
                content.longitude
              ) AS `coordinates`,
              content.ts_create,
              content.ts_update
            FROM content
            JOIN location 
              ON location.id = content.location_id
            JOIN content_type
              ON content_type.id = content.content_type_id
            {$whereQueryPart}
        ");

        if (!empty($bindValues)) {
            /** @var PdoBindValue $bindValue */
            foreach ($bindValues as $bindValue) {
                $pdoPrep->bindValue(
                    $bindValue->getName(),
                    $bindValue->getValue(),
                    $bindValue->getPdoParamType()
                );
            }
        }

        $pdoPrep->execute();

        return $pdoPrep->fetchAll(PDO::FETCH_ASSOC);
    }

}