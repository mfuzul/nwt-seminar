<?php
/**
 * Created by PhpStorm.
 * User: z3r0
 * Date: 15.08.19.
 * Time: 15:40
 */

namespace CoreLogic\Repository\TicketGroup;

use AdminBundle\Model\AbstractFetchListRecordsQueryObject;
use CoreLogic\Entity\TicketGroup;
use CoreLogic\Repository\AdminRepositoryInterface;
use CoreLogic\Repository\NwtRepositoryExtension;
use PDO;

/**
 * Class TicketGroupRepository
 * @package CoreLogic\Repository\TicketGroup
 */
class TicketGroupRepository extends NwtRepositoryExtension implements AdminRepositoryInterface
{
    /** {@inheritdoc} */
    protected function entityClass(): string
    {
        return TicketGroup::class;
    }

    /** {@inheritdoc} */
    function fetchListRecords(AbstractFetchListRecordsQueryObject $queryObject = null): array
    {
        $pdoPrep = $this->conn->prepare("
            SELECT
              ticket_group.name,
              ticket_group.ts_create,
              ticket_group.ts_update,
              COUNT(content_ticket_group.ticket_group_id) AS `content_count`
            FROM ticket_group
            LEFT JOIN content_ticket_group
              ON content_ticket_group.ticket_group_id = ticket_group.id
            GROUP BY ticket_group.id
        ");

        $pdoPrep->execute();

        return $pdoPrep->fetchAll(PDO::FETCH_ASSOC);
    }

}