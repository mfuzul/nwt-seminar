<?php
/**
 * Created by PhpStorm.
 * User: z3r0
 * Date: 15.08.19.
 * Time: 16:48
 */

namespace CoreLogic\Repository\ContentType;

use AdminBundle\Model\AbstractFetchListRecordsQueryObject;
use CoreLogic\Entity\ContentType;
use CoreLogic\Repository\AdminRepositoryInterface;
use CoreLogic\Repository\NwtRepositoryExtension;
use PDO;

class ContentTypeRepository extends NwtRepositoryExtension implements AdminRepositoryInterface
{
    /** {@inheritdoc} */
    protected function entityClass(): string
    {
        return ContentType::class;
    }

    /** {@inheritdoc} */
    function fetchListRecords(AbstractFetchListRecordsQueryObject $queryObject = null): array
    {
        $pdoPrep = $this->conn->prepare("
            SELECT
              content_type.name,
              content_type.ts_create,
              content_type.ts_update,
              COUNT(content.id) AS 'content_count'
            FROM content_type
            LEFT JOIN content 
              ON content.content_type_id = content_type.id
            GROUP BY content_type.id
        ");

        $pdoPrep->execute();

        return $pdoPrep->fetchAll(PDO::FETCH_ASSOC);
    }

}