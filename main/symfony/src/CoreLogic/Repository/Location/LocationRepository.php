<?php
/**
 * Created by PhpStorm.
 * User: z3r0
 * Date: 05.08.19.
 * Time: 18:47
 */

namespace CoreLogic\Repository\Location;

use AdminBundle\Model\AbstractFetchListRecordsQueryObject;
use CoreLogic\Entity\Location;
use CoreLogic\Repository\AdminRepositoryInterface;
use CoreLogic\Repository\NwtRepositoryExtension;
use PDO;

/**
 * Class LocationRepository
 * @package CoreLogic\Repository\Location
 */
class LocationRepository extends NwtRepositoryExtension implements AdminRepositoryInterface
{
    /** {@inheritdoc} */
    protected function entityClass(): string
    {
        return Location::class;
    }

    /** {@inheritdoc} */
    public function fetchListRecords(AbstractFetchListRecordsQueryObject $queryObject = null): array
    {
        $pdoPrep = $this->conn->prepare("
            SELECT
              location.id,
              location.name,
              country.name AS `country`,
              CONCAT(location.latitude, ', ', location.longitude) AS `coordinates`
            FROM location
            JOIN country 
              ON country.id = location.country_id
        ");

        $pdoPrep->execute();

        return $pdoPrep->fetchAll(PDO::FETCH_ASSOC);
    }

}