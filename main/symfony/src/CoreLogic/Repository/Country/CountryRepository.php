<?php
/**
 * Created by PhpStorm.
 * User: z3r0
 * Date: 05.08.19.
 * Time: 17:46
 */

namespace CoreLogic\Repository\Country;

use AdminBundle\Model\AbstractFetchListRecordsQueryObject;
use CoreLogic\Repository\AdminRepositoryInterface;
use CoreLogic\Repository\NwtRepositoryExtension;
use CoreLogic\Entity\Country;
use PDO;

/**
 * Class CountryRepository
 * @package CoreLogic\Repository\Country
 */
class CountryRepository extends NwtRepositoryExtension implements AdminRepositoryInterface
{
    /** {@inheritdoc} */
    protected function entityClass(): string
    {
        return Country::class;
    }

    /** {@inheritdoc} */
    function fetchListRecords(AbstractFetchListRecordsQueryObject $queryObject = null): array
    {
        $pdoPrep = $this->conn->prepare("
            SELECT
              country.id,
              country.name,
              country.short_code AS `shortCode`
            FROM country
        ");

        $pdoPrep->execute();

        return $pdoPrep->fetchAll(PDO::FETCH_ASSOC);
    }

}