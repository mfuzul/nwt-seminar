<?php
/**
 * Created by PhpStorm.
 * User: z3r0
 * Date: 31.03.19.
 * Time: 11:59
 */

namespace CoreLogic\Repository;

use Doctrine\DBAL\Connection;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Mapping;

/**
 * Class NwtRepositoryExtension
 * @package CoreLogic\Repository
 */
abstract class NwtRepositoryExtension extends EntityRepository
{
    /** @var Connection */
    protected $conn;

    /**
     * @return string
     */
    abstract protected function entityClass(): string;

    public function __construct(EntityManager $em, Mapping\ClassMetadata $class)
    {
        $this->conn = $em->getConnection();

        parent::__construct($em, $class);
    }
}