<?php
/**
 * Created by PhpStorm.
 * User: z3r0
 * Date: 24.03.19.
 * Time: 12:50
 */

namespace CoreLogic\Repository\User;

use AdminBundle\Model\AbstractFetchListRecordsQueryObject;
use CoreLogic\Entity\User;
use CoreLogic\Repository\AdminRepositoryInterface;
use CoreLogic\Repository\NwtRepositoryExtension;
use PDO;

/**
 * Class UserRepository
 * @package CoreLogic\Repository
 */
class UserRepository extends NwtRepositoryExtension implements AdminRepositoryInterface
{
    /** {@inheritdoc} */
    protected function entityClass(): string
    {
        return User::class;
    }

    /** {@inheritdoc} */
    public function fetchListRecords(AbstractFetchListRecordsQueryObject $queryObject = null): array
    {
        $pdoPrep = $this->conn->prepare("
            SELECT
              fos_user.id,
              CONCAT(fos_user.first_name, ' ', fos_user.last_name) AS 'fullName',
              fos_user.username,
              fos_user.email,
              fos_user.enabled,
              fos_user.locked,
              fos_user.expired,
              fos_user.credentials_expired,
              fos_user.roles
            FROM fos_user
        ");

        $pdoPrep->execute();

        return $pdoPrep->fetchAll(PDO::FETCH_ASSOC);
    }

}