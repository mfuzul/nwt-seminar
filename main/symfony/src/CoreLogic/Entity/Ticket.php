<?php

namespace CoreLogic\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Ticket
 *
 * @ORM\Table(name="ticket", indexes={@ORM\Index(name="fk_content", columns={"content_id"}), @ORM\Index(name="fk_user", columns={"user_id"}), @ORM\Index(name="fk_ticket_group", columns={"ticket_group_id"})})
 * @ORM\Entity
 */
class Ticket
{
    /**
     * @var float
     *
     * @ORM\Column(name="price", type="float", precision=10, scale=2, nullable=false)
     */
    private $price;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="ts_create", type="datetime", nullable=false)
     */
    private $tsCreate = 'CURRENT_TIMESTAMP';

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="ts_paid", type="datetime", nullable=true)
     */
    private $tsPaid;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="ts_update", type="datetime", nullable=true)
     */
    private $tsUpdate;

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \CoreLogic\Entity\Content
     *
     * @ORM\ManyToOne(targetEntity="CoreLogic\Entity\Content")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="content_id", referencedColumnName="id")
     * })
     */
    private $content;

    /**
     * @var \CoreLogic\Entity\FosUser
     *
     * @ORM\ManyToOne(targetEntity="CoreLogic\Entity\FosUser")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     * })
     */
    private $user;

    /**
     * @var \CoreLogic\Entity\TicketGroup
     *
     * @ORM\ManyToOne(targetEntity="CoreLogic\Entity\TicketGroup")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="ticket_group_id", referencedColumnName="id")
     * })
     */
    private $ticketGroup;


}

