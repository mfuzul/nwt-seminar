<?php

namespace CoreLogic\Entity;

use DateTime;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Class Content
 * @package CoreLogic\Entity
 *
 * @ORM\MappedSuperclass()
 */
class Content
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=false)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="address", type="string", length=255, nullable=false)
     */
    private $address;

    /**
     * @var float
     *
     * @ORM\Column(name="latitude", type="float", precision=9, scale=7, nullable=false)
     */
    private $latitude;

    /**
     * @var float
     *
     * @ORM\Column(name="longitude", type="float", precision=9, scale=7, nullable=false)
     */
    private $longitude;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text", length=65535, nullable=false)
     */
    private $description;

    /**
     * @var DateTime
     *
     * @ORM\Column(name="ts_create", type="datetime", nullable=false)
     */
    private $tsCreate = 'CURRENT_TIMESTAMP';

    /**
     * @var DateTime
     *
     * @ORM\Column(name="ts_update", type="datetime", nullable=true)
     */
    private $tsUpdate;

    /**
     * @var \CoreLogic\Entity\Location
     *
     * @ORM\ManyToOne(targetEntity="CoreLogic\Entity\Location")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="location_id", referencedColumnName="id")
     * })
     */
    private $location;

    /**
     * @var \CoreLogic\Entity\ContentType
     *
     * @ORM\ManyToOne(targetEntity="CoreLogic\Entity\ContentType")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="content_type_id", referencedColumnName="id")
     * })
     */
    private $contentType;

    /**
     * @var Collection
     *
     * @ORM\ManyToMany(targetEntity="CoreLogic\Entity\TicketGroup", inversedBy="content")
     * @ORM\JoinTable(name="content_ticket_group",
     *   joinColumns={
     *     @ORM\JoinColumn(name="content_id", referencedColumnName="id")
     *   },
     *   inverseJoinColumns={
     *     @ORM\JoinColumn(name="ticket_group_id", referencedColumnName="id")
     *   }
     * )
     */
    private $ticketGroup;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->ticketGroup = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     *
     * @return Content
     */
    public function setId(int $id): self
    {
        $this->id = $id;

        return $this;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     *
     * @return Content
     */
    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return string
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * @param string $address
     *
     * @return Content
     */
    public function setAddress(string $address): self
    {
        $this->address = $address;

        return $this;
    }

    /**
     * @return float
     */
    public function getLatitude()
    {
        return $this->latitude;
    }

    /**
     * @param float $latitude
     *
     * @return Content
     */
    public function setLatitude(float $latitude): self
    {
        $this->latitude = $latitude;

        return $this;
    }

    /**
     * @return float
     */
    public function getLongitude()
    {
        return $this->longitude;
    }

    /**
     * @param float $longitude
     *
     * @return Content
     */
    public function setLongitude(float $longitude): self
    {
        $this->longitude = $longitude;

        return $this;
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param string $description
     *
     * @return Content
     */
    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    /**
     * @return DateTime
     */
    public function getTsCreate()
    {
        return $this->tsCreate;
    }

    /**
     * @param DateTime $tsCreate
     *
     * @return Content
     */
    public function setTsCreate(DateTime $tsCreate): self
    {
        $this->tsCreate = $tsCreate;

        return $this;
    }

    /**
     * @return DateTime
     */
    public function getTsUpdate()
    {
        return $this->tsUpdate;
    }

    /**
     * @param DateTime $tsUpdate
     *
     * @return Content
     */
    public function setTsUpdate(DateTime $tsUpdate): self
    {
        $this->tsUpdate = $tsUpdate;

        return $this;
    }

    /**
     * @return Location
     */
    public function getLocation()
    {
        return $this->location;
    }

    /**
     * @param Location $location
     *
     * @return Content
     */
    public function setLocation(Location $location): self
    {
        $this->location = $location;

        return $this;
    }

    /**
     * @return ContentType
     */
    public function getContentType()
    {
        return $this->contentType;
    }

    /**
     * @param ContentType $contentType
     *
     * @return Content
     */
    public function setContentType(ContentType $contentType): self
    {
        $this->contentType = $contentType;

        return $this;
    }

    /**
     * @return Collection
     */
    public function getTicketGroup()
    {
        return $this->ticketGroup;
    }

    /**
     * @param Collection $ticketGroup
     *
     * @return Content
     */
    public function setTicketGroup(Collection $ticketGroup): self
    {
        $this->ticketGroup = $ticketGroup;

        return $this;
    }

}

