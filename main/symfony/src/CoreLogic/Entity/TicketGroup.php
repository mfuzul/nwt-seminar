<?php

namespace CoreLogic\Entity;

use DateTime;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * TicketGroup
 *
 * @ORM\Table(name="ticket_group")
 * @ORM\Entity(repositoryClass="CoreLogic\Repository\TicketGroup\TicketGroupRepository")
 */
class TicketGroup implements EntityInterface
{
    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=100, nullable=false)
     */
    private $name;

    /**
     * @var DateTime
     *
     * @ORM\Column(name="ts_create", type="datetime", nullable=false)
     */
    private $tsCreate = 'CURRENT_TIMESTAMP';

    /**
     * @var DateTime
     *
     * @ORM\Column(name="ts_update", type="datetime", nullable=true)
     */
    private $tsUpdate;

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var Collection
     *
     * @ORM\ManyToMany(targetEntity="CoreLogic\Entity\Content", mappedBy="ticketGroup")
     */
    private $content;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->content = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     *
     * @return TicketGroup
     */
    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return DateTime
     */
    public function getTsCreate()
    {
        return $this->tsCreate;
    }

    /**
     * @param DateTime $tsCreate
     *
     * @return TicketGroup
     */
    public function setTsCreate(DateTime $tsCreate): self
    {
        $this->tsCreate = $tsCreate;

        return $this;
    }

    /**
     * @return DateTime
     */
    public function getTsUpdate()
    {
        return $this->tsUpdate;
    }

    /**
     * @param DateTime $tsUpdate
     *
     * @return TicketGroup
     */
    public function setTsUpdate(DateTime $tsUpdate): self
    {
        $this->tsUpdate = $tsUpdate;

        return $this;
    }

    /**
     * @return Collection
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * @param Collection $content
     *
     * @return TicketGroup
     */
    public function setContent(Collection $content): self
    {
        $this->content = $content;

        return $this;
    }

    /**
     * @param Content $content
     *
     * @return TicketGroup
     */
    public function addContent(Content $content): self
    {
        if (!$this->hasContent($content)) {
            $this->content->add($content);
        }

        return $this;
    }

    /**
     * @param Content $content
     *
     * @return TicketGroup
     */
    public function removeContent(Content $content): self
    {
        $this->content->removeElement($content);

        return $this;
    }

    /**
     * @param Content $content
     *
     * @return bool
     */
    public function hasContent(Content $content): bool
    {
        return $this->content->contains($content);
    }

}

