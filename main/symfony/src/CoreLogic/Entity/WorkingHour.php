<?php

namespace CoreLogic\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * WorkingHour
 *
 * @ORM\Table(name="working_hour", indexes={@ORM\Index(name="fk_operational_interval", columns={"operational_interval_id"})})
 * @ORM\Entity
 */
class WorkingHour
{
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="from", type="time", nullable=false)
     */
    private $from;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="to", type="time", nullable=false)
     */
    private $to;

    /**
     * @var boolean
     *
     * @ORM\Column(name="operational_monday", type="boolean", nullable=true)
     */
    private $operationalMonday = '0';

    /**
     * @var boolean
     *
     * @ORM\Column(name="operational_tuesday", type="boolean", nullable=true)
     */
    private $operationalTuesday = '0';

    /**
     * @var boolean
     *
     * @ORM\Column(name="operational_wednesday", type="boolean", nullable=true)
     */
    private $operationalWednesday = '0';

    /**
     * @var boolean
     *
     * @ORM\Column(name="operational_thursday", type="boolean", nullable=true)
     */
    private $operationalThursday = '0';

    /**
     * @var boolean
     *
     * @ORM\Column(name="operational_friday", type="boolean", nullable=true)
     */
    private $operationalFriday = '0';

    /**
     * @var boolean
     *
     * @ORM\Column(name="operational_saturday", type="boolean", nullable=true)
     */
    private $operationalSaturday = '0';

    /**
     * @var boolean
     *
     * @ORM\Column(name="operational_sunday", type="boolean", nullable=true)
     */
    private $operationalSunday = '0';

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="ts_create", type="datetime", nullable=false)
     */
    private $tsCreate = 'CURRENT_TIMESTAMP';

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="ts_update", type="datetime", nullable=true)
     */
    private $tsUpdate;

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \CoreLogic\Entity\OperationalInterval
     *
     * @ORM\ManyToOne(targetEntity="CoreLogic\Entity\OperationalInterval")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="operational_interval_id", referencedColumnName="id")
     * })
     */
    private $operationalInterval;


}

