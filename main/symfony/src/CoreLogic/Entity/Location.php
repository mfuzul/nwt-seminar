<?php

namespace CoreLogic\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Location
 *
 * @ORM\Table(name="location", indexes={@ORM\Index(name="fk_country", columns={"country_id"})})
 * @ORM\Entity(repositoryClass="CoreLogic\Repository\Location\LocationRepository")
 */
class Location implements EntityInterface
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=100, nullable=false)
     */
    private $name;

    /**
     * @var float
     *
     * @ORM\Column(name="latitude", type="float", precision=9, scale=7, nullable=false)
     */
    private $latitude;

    /**
     * @var float
     *
     * @ORM\Column(name="longitude", type="float", precision=9, scale=7, nullable=false)
     */
    private $longitude;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="ts_create", type="datetime", nullable=false)
     */
    private $tsCreate = 'CURRENT_TIMESTAMP';

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="ts_update", type="datetime", nullable=true)
     */
    private $tsUpdate;

    /**
     * @var \CoreLogic\Entity\Country
     *
     * @ORM\ManyToOne(targetEntity="CoreLogic\Entity\Country")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="country_id", referencedColumnName="id")
     * })
     */
    private $country;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     *
     * @return Location
     */
    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return float
     */
    public function getLatitude()
    {
        return $this->latitude;
    }

    /**
     * @param float $latitude
     *
     * @return Location
     */
    public function setLatitude(float $latitude): self
    {
        $this->latitude = $latitude;

        return $this;
    }

    /**
     * @return float
     */
    public function getLongitude()
    {
        return $this->longitude;
    }

    /**
     * @param float $longitude
     *
     * @return Location
     */
    public function setLongitude(float $longitude): self
    {
        $this->longitude = $longitude;

        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getTsCreate()
    {
        return $this->tsCreate;
    }

    /**
     * @param \DateTime $tsCreate
     *
     * @return Location
     */
    public function setTsCreate(\DateTime $tsCreate): self
    {
        $this->tsCreate = $tsCreate;

        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getTsUpdate()
    {
        return $this->tsUpdate;
    }

    /**
     * @param \DateTime $tsUpdate
     *
     * @return Location
     */
    public function setTsUpdate(\DateTime $tsUpdate): self
    {
        $this->tsUpdate = $tsUpdate;

        return $this;
    }

    /**
     * @return Country
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * @param Country $country
     *
     * @return Location
     */
    public function setCountry(Country $country): self
    {
        $this->country = $country;

        return $this;
    }

    /**
     * @return string
     */
    public function getCoordinates(): string
    {
        $latitude = $this->getLatitude();
        $longitude = $this->getLongitude();

        if ($latitude && $longitude) {
            return "{$latitude},{$longitude}";
        }

        return '';
    }

}

