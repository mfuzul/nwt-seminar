<?php

namespace CoreLogic\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * OperationalInterval
 *
 * @ORM\Table(name="operational_interval", indexes={@ORM\Index(name="fk_content", columns={"content_id"})})
 * @ORM\Entity
 */
class OperationalInterval
{
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="from", type="datetime", nullable=false)
     */
    private $from;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="to", type="datetime", nullable=false)
     */
    private $to;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="ts_create", type="datetime", nullable=false)
     */
    private $tsCreate = 'CURRENT_TIMESTAMP';

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="ts_update", type="datetime", nullable=true)
     */
    private $tsUpdate;

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \CoreLogic\Entity\Content
     *
     * @ORM\ManyToOne(targetEntity="CoreLogic\Entity\Content")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="content_id", referencedColumnName="id")
     * })
     */
    private $content;


}

