<?php
/**
 * Created by PhpStorm.
 * User: z3r0
 * Date: 15.08.19.
 * Time: 17:36
 */

namespace CoreLogic\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Class ContentConcert
 * @package CoreLogic\Entity
 *
 * @ORM\Table(name="content")
 * @ORM\Entity(repositoryClass="CoreLogic\Repository\Content\ContentRepository")
 */
class ContentConcert extends Content implements EntityInterface
{
}