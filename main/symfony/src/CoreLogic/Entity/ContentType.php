<?php

namespace CoreLogic\Entity;

use DateTime;
use Doctrine\ORM\Mapping as ORM;

/**
 * ContentType
 *
 * @ORM\Table(name="content_type")
 * @ORM\Entity(repositoryClass="CoreLogic\Repository\ContentType\ContentTypeRepository")
 */
class ContentType implements EntityInterface
{
    /** @var int */
    const CONTENT_TYPE_MUSEUM = 1;

    /** @var int */
    const CONTENT_TYPE_CONCERT = 2;

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=100, nullable=false)
     */
    private $name;

    /**
     * @var DateTime
     *
     * @ORM\Column(name="ts_create", type="datetime", nullable=false)
     */
    private $tsCreate = 'CURRENT_TIMESTAMP';

    /**
     * @var DateTime
     *
     * @ORM\Column(name="ts_update", type="datetime", nullable=true)
     */
    private $tsUpdate;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     *
     * @return ContentType
     */
    public function setId(int $id): self
    {
        $this->id = $id;

        return $this;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     *
     * @return ContentType
     */
    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return DateTime
     */
    public function getTsCreate()
    {
        return $this->tsCreate;
    }

    /**
     * @param DateTime $tsCreate
     *
     * @return ContentType
     */
    public function setTsCreate(DateTime $tsCreate): self
    {
        $this->tsCreate = $tsCreate;

        return $this;
    }

    /**
     * @return DateTime
     */
    public function getTsUpdate()
    {
        return $this->tsUpdate;
    }

    /**
     * @param DateTime $tsUpdate
     *
     * @return ContentType
     */
    public function setTsUpdate(DateTime $tsUpdate): self
    {
        $this->tsUpdate = $tsUpdate;

        return $this;
    }

}

