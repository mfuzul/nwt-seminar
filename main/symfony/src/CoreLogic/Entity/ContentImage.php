<?php

namespace CoreLogic\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ContentImage
 *
 * @ORM\Table(name="content_image", indexes={@ORM\Index(name="fk_content", columns={"content_id"})})
 * @ORM\Entity
 */
class ContentImage
{
    /**
     * @var string
     *
     * @ORM\Column(name="file_name", type="string", length=255, nullable=false)
     */
    private $fileName;

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \CoreLogic\Entity\Content
     *
     * @ORM\ManyToOne(targetEntity="CoreLogic\Entity\Content")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="content_id", referencedColumnName="id")
     * })
     */
    private $content;


}

