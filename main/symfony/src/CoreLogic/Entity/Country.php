<?php

namespace CoreLogic\Entity;

use DateTime;
use Doctrine\ORM\Mapping as ORM;

/**
 * Country
 *
 * @ORM\Table(name="country")
 * @ORM\Entity(repositoryClass="CoreLogic\Repository\Country\CountryRepository")
 */
class Country
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=100, nullable=false)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="short_code", type="string", length=5, nullable=false)
     */
    private $shortCode;

    /**
     * @var DateTime
     *
     * @ORM\Column(name="ts_create", type="datetime", nullable=false, options={"default": "CURRENT_TIMESTAMP"})
     */
    private $tsCreate;

    /**
     * @var DateTime
     *
     * @ORM\Column(name="ts_update", type="datetime", nullable=true)
     */
    private $tsUpdate;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name): void
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getShortCode()
    {
        return $this->shortCode;
    }

    /**
     * @param string $shortCode
     */
    public function setShortCode(string $shortCode): void
    {
        $this->shortCode = $shortCode;
    }

    /**
     * @return DateTime
     */
    public function getTsCreate()
    {
        return $this->tsCreate;
    }

    /**
     * @param DateTime $tsCreate
     */
    public function setTsCreate(DateTime $tsCreate): void
    {
        $this->tsCreate = $tsCreate;
    }

    /**
     * @return DateTime
     */
    public function getTsUpdate()
    {
        return $this->tsUpdate;
    }

    /**
     * @param DateTime $tsUpdate
     */
    public function setTsUpdate(DateTime $tsUpdate): void
    {
        $this->tsUpdate = $tsUpdate;
    }

}

