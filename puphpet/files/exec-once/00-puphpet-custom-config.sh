#!/usr/bin/env bash

# Ubuntu 16.04 custom config file

echo "Running custom script..."

if [[ ! -d '/vagrant/--gitignore/vm-ubuntu' ]]; then
    mkdir -p /vagrant/--gitignore/vm-ubuntu
fi

cp -r --parents /etc/apache2/sites-available/*.conf /vagrant/--gitignore/vm-ubuntu/
cp -r /vagrant/vm-custom-config/* /